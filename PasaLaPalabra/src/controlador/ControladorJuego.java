package controlador;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Robot;
import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.ExcepcionProblemasRosco;
import modelo.Juego;
import modelo.Jugador;
import modelo.Maquina;
import modelo.Player;
import modelo.Pregunta;
import modelo.PreguntaDao;
import modelo.RankingDAO;
import modelo.Reproductor;

import vista.VistaRosco;
import vista.vistaJuego;

public class ControladorJuego implements ActionListener, KeyListener {

	private vistaJuego vista;
	private Juego juego;
	private Reproductor repro;
	private PreguntaDao preguntas;
	private String respuesta;
	private Integer indice1;
	private Integer indice2;
	private ArrayList<Pregunta> preguntasJugador1;
	private ArrayList<Pregunta> preguntasJugador2;
	private Integer tiempo1;
	private Integer tiempo2;
	private Integer contador1 = 0;
	private Integer contador2 = 0;
	private String crono;
	private RankingDAO ranking;

	private VistaRosco rocote = new VistaRosco();

	public ControladorJuego(Juego juego) {
		super();
		preguntas = new PreguntaDao();

		this.vista = new vistaJuego(this);
		this.vista.setVisible(true);
		this.setJuego(juego);
		this.setRepro(new Reproductor());
		this.setIndice1(0);
		this.setIndice2(0);
		try {//Propagacion de excepcion  "Excepcion problema rosco"(controla la cantidad de preguntas que entran al rosco)
            this.setPreguntasJugador1(juego.obtengopreguntas());
            this.setPreguntasJugador2(juego.obtengopreguntas());
        } catch (ExcepcionProblemasRosco e) {
            JOptionPane.showMessageDialog(getVista(), e.getMessage());
            vista.dispose();
        }
		iniciarComponentesJuego();
		iniciarJuego();
	}

	public PreguntaDao getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(PreguntaDao preguntas) {
		this.preguntas = preguntas;
	}

	public Reproductor getRepro() {
		return repro;
	}

	public void setRepro(Reproductor repro) {
		this.repro = repro;
	}

	public vistaJuego getVista() {
		return vista;
	}

	public void setVista(vistaJuego vista) {
		this.vista = vista;
	}

	public Juego getJuego() {
		return juego;
	}

	public void setJuego(Juego juego) {
		this.juego = juego;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public Integer getIndice1() {
		return indice1;
	}

	public void setIndice1(Integer indice) {
		this.indice1 = indice;
	}

	public Integer getIndice2() {
		return indice2;
	}

	public void setIndice2(Integer indice2) {
		this.indice2 = indice2;
	}

	

	public Integer getTiempo1() {
		return tiempo1;
	}

	public void setTiempo1(Integer tiempo1) {
		this.tiempo1 = tiempo1;
	}

	public Integer getTiempo2() {
		return tiempo2;
	}

	public void setTiempo2(Integer tiempo2) {
		this.tiempo2 = tiempo2;
	}

	public VistaRosco getRocote() {
		return rocote;
	}

	public void setRocote(VistaRosco rocote) {
		this.rocote = rocote;
	}

	public ArrayList<Pregunta> getPreguntasJugador1() {
		return preguntasJugador1;
	}

	public void setPreguntasJugador1(ArrayList<Pregunta> preguntasJugador1) {
		this.preguntasJugador1 = preguntasJugador1;
	}

	public ArrayList<Pregunta> getPreguntasJugador2() {
		return preguntasJugador2;
	}

	public void setPreguntasJugador2(ArrayList<Pregunta> preguntasJugador2) {
		this.preguntasJugador2 = preguntasJugador2;
	}

	public RankingDAO getRanking() {
		return ranking;
	}

	public void setRanking(RankingDAO ranking) {
		this.ranking = ranking;
	}
//inician  componentes de la partida(setteo de componentes) 
	public void iniciarComponentesJuego() {

		juego.getPreguntas();

		String texto = juego.getJugador1().getNombre();
		this.juego.getJugador1().setEstadoJugador(true);
		if(this.juego.getJugador2()!= null) {
			this.juego.getJugador2().setEstadoJugador(false);
		}else {
			this.juego.getCpu().setEstadoJugador(false);
		}
		
		vista.getLblNewLabelugador1().setText(texto);
       
		String texto2 = "";
		if (juego.getJugador2() != null) {
			texto2 = juego.getJugador2().getNombre();
			vista.getLblNewLabelJugador2().setText(texto2);

		} else {

			vista.getLblNewLabelJugador2().setText("CPU");
            
		}

		String crono = juego.getTiempoJugador1().toString();
		crono = juego.getTiempoJugador1().toString();
		vista.getLblTiempo2().setText(crono);
		vista.getLblTiempo1().setText(crono);

		int numer2 = 0;
		String num = Integer.toString(numer2);
		vista.getLblRtsC2_1().setText(num);
		vista.getLblRtsC2().setText(num);

		String ruta = juego.getMusica();

		switch (ruta) {
		case "Musica":
			System.out.println("No se agrega musica");
			break;
		case "Cancion 1":
			Reproductor re = new Reproductor();
			try {
				re.abrir("Musica\\Pista_1.mp3");
				re.play();
			} catch (BasicPlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			break;
		case "Cancion 2":
			System.out.println("Se agrega cancion 2");
			Reproductor re1 = new Reproductor();
			try {
				re1.abrir("Musica\\Pista_2.mp3");
				re1.play();
			} catch (BasicPlayerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		default:
			System.out.println("no se agrega musica");
			break;
		}

	}

	public void iniciarJuego() {
//comienza la partida ejecutando los metodos 
		
		// Instancio las preguntas aca para mostrar en el Jlabel la primer pregunta

		this.getVista().getLblPregunta()
				.setText("<html>" + this.getPreguntasJugador1().get(0).getPregunta() + "<html>");
		setterosco(this.getPreguntasJugador1());
		if(juego.getJugador2()!= null) {
	
	JOptionPane.showMessageDialog(getVista(),"Presione ENTER: para responder ; CONTROL: para pasar");// instrucciones del juego para dos jugadores
	cronometro(juego.getJugador1(),juego.getJugador2());//cronometros cuando son dos jugadores persona
}else {
	cronometro(juego.getJugador1(),juego.getCpu());// cronometro cuando es un jugador persona y maquina
	//instrucciones del juego para jugador maquina y persona
	JOptionPane.showMessageDialog(getVista(),"Presione ENTER: para responder ; CONTROL: para pasar ; ENTER : para que responda la maquina");
}
		
		

	}
//incrementa el contador de preguntas correctas que se muestran en la pantalla
	public void incrementoContador(Pregunta pregunta ,Jugador ju,Player ju2) {

		if (pregunta.getEstadoP().equals("correcto")) {
			if (ju.getEstadoJugador().equals(true)) {
				contador1 = contador1 + 1;
				String num = Integer.toString(contador1);
				vista.getLblRtsC2_1().setText(num);
			} else if (ju2.getEstadoJugador().equals(true)) {
				contador2 = contador2 + 1;
				String num = Integer.toString(contador2);
				vista.getLblRtsC2().setText(num);
			}

		}
	}
//cronometro de la partida para ambos jugadores
	public void cronometro(Jugador ju,Player ju2) {// cronometro
		crono = juego.getTiempoJugador1().toString();
		Timer tiempo = new Timer();

		TimerTask tasj = new TimerTask() {

			Integer cronometro1 = Integer.valueOf(crono);

			Integer cronometro2 = Integer.valueOf(crono);

			@Override
			public void run() {

				if (ju.getEstadoJugador().equals(true)
						|| ju.getEstadoPartida().equals(false)) {
					String t1 = cronometro1.toString();
					vista.getLblTiempo1().setText(t1);
					if (cronometro1 >= 1) {
						cronometro1 = cronometro1 - 1;
						setTiempo1(cronometro1);
					}

				} else if (ju2.getEstadoJugador().equals(true)
						|| ju2.getEstadoPartida().equals(false)) {
					String t2 = cronometro2.toString();
					vista.getLblTiempo2().setText(t2);
					if (cronometro2 >= 1) {
						cronometro2 = cronometro2 - 1;
						setTiempo2(cronometro2);
					}
					
				}
			}
		};
		tiempo.schedule(tasj, 2000, 1000);
	}

	public void setterosco(ArrayList<Pregunta> listapregunta) {// settea rosco por primera vez el rosco de los jugadores

		for (Pregunta pregunta2 : listapregunta) {
			cambiarColor(pregunta2);
		}
	}
//Cambia color de las respuesta deacuerdo al estado
	public void cambiarColor(Pregunta pregunta) {

		if (pregunta.getLetra() == 'A') {
			vista.getLblA().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'B') {
			vista.getLblB().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'C') {
			vista.getLblC().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'D') {
			vista.getLblD().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'E') {
			vista.getLblE().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'F') {
			vista.getLblF().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'G') {

			vista.getLblG().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'H') {

			vista.getLblH().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'I') {

			vista.getLblI().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'J') {
			vista.getLblJ().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'K') {
			vista.getLblK().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'L') {
			vista.getLblL().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'M') {
			vista.getLblM().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'N') {
			vista.getLblN().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'O') {
			vista.getLblO().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'P') {

			vista.getLblP().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'Q') {
			vista.getLblQ().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'R') {
			vista.getLblR().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'S') {
			vista.getLblS().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'T') {
			vista.getLblT().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'U') {
			vista.getLblU().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'V') {
			vista.getLblV().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'W') {
			vista.getLblW().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'X') {
			vista.getLblX().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'Y') {
			vista.getLblY().setIcon(rocote.cambioColor(pregunta));
		}
		if (pregunta.getLetra() == 'Z') {
			vista.getLblZ().setIcon(rocote.cambioColor(pregunta));
		}
	}
//muestra y agrupa las letras  deacuerdo al estado de la pregunta
	public void muestroLetras1(ArrayList<Pregunta> preguntaa) {
		String bien = "Bien: ";
		String mal = "Mal: ";
		String paso = "Paso: ";

		for (Pregunta pregunta : preguntaa) {
			switch (pregunta.getEstadoP()) {
			case "correcto":
				bien = bien + pregunta.getLetra() + ", ";
				vista.getLblcorractas().setText("<html>" + bien + "<html>");
				break;
			case "incorrecto":
				mal = mal + pregunta.getLetra() + ", ";
				vista.getLblincorrectas().setText("<html>" + mal + "<html>");
				break;
			case "paso":
				paso = paso + pregunta.getLetra() + ",";
				vista.getLblPaso().setText("<html>" + paso + "<html>");
				break;

			default:
				break;

			}

		}

	}

	public void muestroLetras2(ArrayList<Pregunta> preguntaa) {
		String bien = "Bien: ";
		String mal = "Mal: ";
		String paso = "Paso: ";

		for (Pregunta pregunta : preguntaa) {
			switch (pregunta.getEstadoP()) {
			case "correcto":
				bien = bien + pregunta.getLetra() + ", ";
				vista.getLblcorractas_1().setText("<html>" + bien + "<html>");
				break;
			case "incorrecto":
				mal = mal + pregunta.getLetra() + ", ";
				vista.getLblincorrectas_1().setText("<html>" + mal + "<html>");
				break;
			case "paso":
				paso = paso + pregunta.getLetra() + ",";
				vista.getLblPaso_1().setText("<html>" + paso + "<html>");
				break;

			default:
				break;

			}

		}

	}
//setea el estado de los jugadores dependiendo de como respondieron
	public void setteoestados(Player ju, Player ju2) {

		if (ju.getEstadoJugador().equals(true)) {
			ju2.setEstadoJugador(false);
		} else if (ju.getEstadoJugador().equals(false) && ju2.getEstadoPartida()) {
			ju2.setEstadoJugador(true);
		} else {
			ju.setEstadoJugador(true);
		}

	}
// retorna el indice de la pregunta que le toca responder 
	public Integer pasoRosco(ArrayList<Pregunta> listaRosco, Integer indi) {
		int contador = 0;
		for (Pregunta pregunta : listaRosco) {
			if (pregunta.getEstadoP().equals("correcto") || (pregunta.getEstadoP().equals("incorrecto"))) {
				contador++;
			}
		}
		if (contador == 26) {
System.out.println("paso rosco "+ indi);
			return 0;
		}
		if (listaRosco.get(indi).getEstadoP() != "sin responder") {

			while (listaRosco.get(indi).getEstadoP().equals("correcto")
					|| listaRosco.get(indi).getEstadoP().equals("incorrecto")) {
				if (indi + 1 <= listaRosco.size() - 1) {
					indi = indi + 1;

				} else {
					indi = 0;
				}
			}
		}
		return indi;
	}
	public void turnoMaquina(ArrayList<Pregunta> listamaquina,Player mq, Integer indiM,Jugador ju) {
		//metodo para que responda la maquina 
	mq.responder(listamaquina.get(indiM), "");
	cambiarColor(listamaquina.get(indiM));
		aumentoPreguntas(listamaquina, indiM);
		if (listamaquina.get(indiM).getEstadoP().equals("correcto")) {
			incrementoContador(listamaquina.get(indiM),ju,mq);
		}
		
		
	}

	public void turnoJugador(ArrayList<Pregunta> listapre, Jugador ju, Integer indi, Player ju2,
			ArrayList<Pregunta> listapre2, Integer indi2) { // metodo para responder mediante teclado;
//metodo para que responda el jugador persona
		ju.setRespuesta(vista.getTxtRespuesta().getText());
		ju.responder(listapre.get(indi), ju.getRespuesta());
		cambiarColor(listapre.get(indi));
		if (listapre.get(indi).getEstadoP().equals("correcto")) {
			incrementoContador(listapre.get(indi),ju,ju2);
		}
		setteoestados(ju, ju2);
		vista.getTxtRespuesta().setText("");
		precionaEnter(ju, listapre, indi, listapre2, indi2, ju2);
		System.out.println("indice pregunta responde " + indi);

	}

	public Integer aumentoPreguntas(ArrayList<Pregunta> listapre, Integer indi) {// aumenta el indice de la  pregunta del jugador

		if (indi + 1 <= listapre.size() - 1) {
			indi = indi + 1;
			System.out.println("indice1.2 " + indi);

		} else {
			indi = 0;
		}
		System.out.println("indice en metodo 1" + indi);
		return indi;
	}

	public void jugadorPasa(Jugador ju1, ArrayList<Pregunta> listapreguntas, Integer indi, Player ju2) {
//metodo cuando el jugador pasa la pregunta 
		listapreguntas.get(indi).setEstadoP("paso");
		cambiarColor(listapreguntas.get(indi));

		if (ju2.getEstadoPartida()) {
			ju1.setEstadoJugador(false);
			setteoestados(ju1, ju2);
		}

	}

	public void precionaEnter(Player ju1, ArrayList<Pregunta> listajugadorp, Integer indi1,
			ArrayList<Pregunta> listajuagadorp2, Integer indi2, Player ju2) {// metodo para aumentar y mostrar las
																				// preguntas respondidas

		if (ju1.getEstadoJugador().equals(true) && ju1.getEstadoPartida()) {
			indi1 = (aumentoPreguntas(listajugadorp, indi1));
			indi1 = (pasoRosco(listajugadorp, indi1));
			System.out.println("indice preciona enter " + indi1);
			this.getVista().getLblPregunta().setText("<html>" + listajugadorp.get(indi1).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla().setText(listajugadorp.get(indi1).getLetra().toString());

		} else {
			indi1 = (aumentoPreguntas(listajugadorp, indi1));
			if (ju2.getEstadoPartida()) {
				this.getVista().getLblPregunta()
						.setText("<html>" + listajuagadorp2.get(indi2).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla().setText(listajuagadorp2.get(indi2).getLetra().toString());
			}

		}
	}
public void partidaJugadorMaquinaEnter()  {
	// metodo para la partida del jugador con maquina y jugador ... 
	
	if (juego.getJugador1().getEstadoJugador()) {
		juego.getJugador1().setEstadoPartida(juego.getJugador1().terminaJugador(getTiempo1(), getPreguntasJugador1()));

		if (juego.getJugador1().getEstadoPartida().equals(false)) {

			setterosco(this.getPreguntasJugador2());
			this.getVista().getLblPregunta().setText(
					"<html>" + this.getPreguntasJugador2().get(this.getIndice2()).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla()
					.setText(this.getPreguntasJugador2().get(this.getIndice2()).getLetra().toString());
		
			juego.getCpu().setEstadoJugador(true);
			juego.getJugador1().setEstadoJugador(false);
			
		}
		if (juego.getJugador1().getEstadoJugador() && juego.getJugador1().getEstadoPartida()) {
			turnoJugador(this.getPreguntasJugador1(), juego.getJugador1(), this.getIndice1(),
					juego.getCpu(), this.getPreguntasJugador2(), this.getIndice2());
			if (juego.getJugador1().getEstadoJugador().equals(false)
					&& juego.getCpu().getEstadoPartida()) {
				setterosco(this.getPreguntasJugador2());
				
			}
			this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
			muestroLetras1(this.preguntasJugador1);
			this.getVista().getLblletraQue()
			.setText("Letra: "+this.getPreguntasJugador1().get(getIndice1()).getLetra().toString());
		}	

	}else if(juego.getCpu().getEstadoJugador()){
	
		juego.getCpu().setEstadoPartida(juego.getCpu().maquinaTermina( getPreguntasJugador2(),getTiempo2()));
		if (juego.getCpu().getEstadoPartida().equals(false)) {

			setterosco(this.getPreguntasJugador1());
			this.getVista().getLblPregunta().setText(
					"<html>" + this.getPreguntasJugador1().get(this.getIndice1()).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla()
					.setText(this.getPreguntasJugador1().get(this.getIndice1()).getLetra().toString());
			vista.getTxtRespuesta().setText("");
			juego.getJugador1().setEstadoJugador(true);
			juego.getCpu().setEstadoJugador(false);
		
		}	
		if (juego.getCpu().getEstadoJugador() && juego.getCpu().getEstadoPartida()) {
			turnoMaquina(getPreguntasJugador2(), juego.getCpu(),this.getIndice2(),juego.getJugador1());
			setteoestados( juego.getCpu(),juego.getJugador1());	
			precionaEnter(juego.getCpu(), this.getPreguntasJugador2(), this.getIndice2(),this.getPreguntasJugador1(),this.getIndice1(),juego.getJugador1());
			if (juego.getCpu().getEstadoJugador().equals(false)
					&& juego.getJugador1().getEstadoPartida()) {
				setterosco(this.getPreguntasJugador1());
				this.setIndice2(aumentoPreguntas(getPreguntasJugador2(), this.getIndice2()));
			}
			System.out.println("indice maquina: "+ this.getIndice2());
			this.setIndice2(pasoRosco(this.getPreguntasJugador2(), this.getIndice2()));
			System.out.println("indice maquina salee: "+ this.getIndice2());
			muestroLetras2(this.preguntasJugador2);
			this.getVista().getLblletraQ2()
			.setText("letra : "+ this.getPreguntasJugador2().get(getIndice2()).getLetra().toString());
		}
		
	}
}
public void partidaJugadorMaquinaControl() {
	//metodo para pasas del jugador jugando con la maquina
	if (juego.getJugador1().getEstadoJugador().equals(true)) {

		juego.getJugador1()
				.setEstadoPartida(juego.getJugador1().terminaJugador(getTiempo1(), getPreguntasJugador1()));
		if (juego.getJugador1().getEstadoPartida().equals(false)) {

			setterosco(this.getPreguntasJugador2());
			this.getVista().getLblPregunta().setText(
					"<html>" + this.getPreguntasJugador2().get(this.getIndice2()).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla()
					.setText(this.getPreguntasJugador2().get(this.getIndice2()).getLetra().toString());
		
			juego.getCpu().setEstadoJugador(true);
			juego.getJugador1().setEstadoJugador(false);
		}

		jugadorPasa(juego.getJugador1(), this.getPreguntasJugador1(), this.getIndice1(),
				juego.getCpu());
		if (juego.getCpu().getEstadoPartida()) {
			juego.getCpu().setEstadoPartida(juego.getCpu().maquinaTermina(getPreguntasJugador2(), getTiempo2()));
			setterosco(this.getPreguntasJugador2());
			this.getVista().getLblPregunta()
					.setText("<html>" + this.preguntasJugador2.get(getIndice2()).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla()
					.setText(this.getPreguntasJugador2().get(getIndice2()).getLetra().toString());
		} else {

			this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
			muestroLetras1(this.preguntasJugador1);
			this.getVista().getLblPregunta().setText(
					"<html>" + this.preguntasJugador1.get(getIndice1() + 1).getPregunta() + "<html>");
			this.getVista().getLblLetraPantalla()
					.setText(this.getPreguntasJugador1().get(getIndice1() + 1).getLetra().toString());
		}

		this.setIndice1(aumentoPreguntas(this.preguntasJugador1, this.indice1));

		this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
		muestroLetras1(this.preguntasJugador1);
		this.getVista().getLblletraQue()
		.setText("Letra: "+this.getPreguntasJugador1().get(getIndice1()).getLetra().toString());
}}
	public void partidaJugadorPersonaEnter() {
	//partida con dos jugadores persona 	
		if (juego.getJugador1().getEstadoJugador()) {
			juego.getJugador1()
					.setEstadoPartida(juego.getJugador1().terminaJugador(getTiempo1(), getPreguntasJugador1()));

			if (juego.getJugador1().getEstadoPartida().equals(false)) {

				setterosco(this.getPreguntasJugador2());
				this.getVista().getLblPregunta().setText(
						"<html>" + this.getPreguntasJugador2().get(this.getIndice2()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador2().get(this.getIndice2()).getLetra().toString());
				vista.getTxtRespuesta().setText("");
				juego.getJugador2().setEstadoJugador(true);
				juego.getJugador1().setEstadoJugador(false);
			}
			if (juego.getJugador1().getEstadoJugador() && juego.getJugador1().getEstadoPartida()) {
				turnoJugador(this.getPreguntasJugador1(), juego.getJugador1(), this.getIndice1(),
						juego.getJugador2(), this.getPreguntasJugador2(), this.getIndice2());
				if (juego.getJugador1().getEstadoJugador().equals(false)
						&& juego.getJugador2().getEstadoPartida()) {
					setterosco(this.getPreguntasJugador2());
				}
				this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
				muestroLetras1(this.preguntasJugador1);
				this.getVista().getLblletraQue()
				.setText("Letra: "+this.getPreguntasJugador1().get(getIndice1()).getLetra().toString());
			}	

		} else if (juego.getJugador2().getEstadoJugador()) {

			juego.getJugador2()
					.setEstadoPartida(juego.getJugador2().terminaJugador(getTiempo2(), getPreguntasJugador2()));

			if (juego.getJugador2().getEstadoPartida().equals(false)) {

				setterosco(this.getPreguntasJugador1());
				this.getVista().getLblPregunta().setText(
						"<html>" + this.getPreguntasJugador1().get(this.getIndice1()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador1().get(this.getIndice1()).getLetra().toString());
				vista.getTxtRespuesta().setText("");
				juego.getJugador1().setEstadoJugador(true);
				juego.getJugador2().setEstadoJugador(false);
			}
			if (juego.getJugador2().getEstadoJugador() && juego.getJugador2().getEstadoPartida()) {
				turnoJugador(this.getPreguntasJugador2(), juego.getJugador2(), this.getIndice2(),
						juego.getJugador1(), this.getPreguntasJugador1(), this.getIndice1());
				if (juego.getJugador2().getEstadoJugador().equals(false)
						&& juego.getJugador1().getEstadoPartida()) {
					setterosco(this.getPreguntasJugador1());
					juego.getJugador2().setEstadoJugador(false);
				}
				this.setIndice2(pasoRosco(this.getPreguntasJugador2(), this.getIndice2()));
				muestroLetras2(this.preguntasJugador2);
				this.getVista().getLblletraQ2()
				.setText("Letra: "+this.getPreguntasJugador2().get(getIndice2()).getLetra().toString());
			} 

		}	

	}
	public void partidaPersonaControl() {
		if (juego.getJugador1().getEstadoJugador().equals(true)) {

			juego.getJugador1()
					.setEstadoPartida(juego.getJugador1().terminaJugador(getTiempo1(), getPreguntasJugador1()));
			if (juego.getJugador1().getEstadoPartida().equals(false)) {

				setterosco(this.getPreguntasJugador2());
				this.getVista().getLblPregunta().setText(
						"<html>" + this.getPreguntasJugador2().get(this.getIndice2()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador2().get(this.getIndice2()).getLetra().toString());
				vista.getTxtRespuesta().setText("");
				juego.getJugador2().setEstadoJugador(true);
				juego.getJugador1().setEstadoJugador(false);
			}

			jugadorPasa(juego.getJugador1(), this.getPreguntasJugador1(), this.getIndice1(),
					juego.getJugador2());
			if (juego.getJugador2().getEstadoPartida()) {
				setterosco(this.getPreguntasJugador2());
				this.getVista().getLblPregunta()
						.setText("<html>" + this.preguntasJugador2.get(getIndice2()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador2().get(getIndice2()).getLetra().toString());
			} else {

				this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
				muestroLetras1(this.preguntasJugador1);
				this.getVista().getLblPregunta().setText(
						"<html>" + this.preguntasJugador1.get(getIndice1() + 1).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador1().get(getIndice1() + 1).getLetra().toString());
			}

			this.setIndice1(aumentoPreguntas(this.preguntasJugador1, this.indice1));

			this.setIndice1(pasoRosco(this.getPreguntasJugador1(), this.getIndice1()));
			muestroLetras1(this.preguntasJugador1);
			this.getVista().getLblletraQue()
			.setText("Letra: "+this.getPreguntasJugador1().get(getIndice1()).getLetra().toString());
		} else if (juego.getJugador2().getEstadoJugador().equals(true)) {
			juego.getJugador2()
					.setEstadoPartida(juego.getJugador2().terminaJugador(getTiempo2(), getPreguntasJugador2()));

			if (juego.getJugador2().getEstadoPartida().equals(false)) {

				setterosco(this.getPreguntasJugador1());
				this.getVista().getLblPregunta().setText(
						"<html>" + this.getPreguntasJugador1().get(this.getIndice1()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador1().get(this.getIndice1()).getLetra().toString());
				vista.getTxtRespuesta().setText("");
				juego.getJugador1().setEstadoJugador(true);
				juego.getJugador2().setEstadoJugador(false);
			}
			jugadorPasa(juego.getJugador2(), this.getPreguntasJugador2(), this.getIndice2(),
					juego.getJugador1());
			if (juego.getJugador1().getEstadoPartida()) {

				setterosco(this.getPreguntasJugador1());
				this.getVista().getLblPregunta()
						.setText("<html>" + this.preguntasJugador1.get(getIndice1()).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador1().get(getIndice1()).getLetra().toString());
			} else {

				this.setIndice2(pasoRosco(this.getPreguntasJugador2(), this.getIndice2()));
				muestroLetras2(this.preguntasJugador2);

				this.getVista().getLblPregunta().setText(
						"<html>" + this.preguntasJugador2.get(getIndice2() + 1).getPregunta() + "<html>");
				this.getVista().getLblLetraPantalla()
						.setText(this.getPreguntasJugador2().get(getIndice2() + 1).getLetra().toString());

			}
			this.setIndice2(aumentoPreguntas(getPreguntasJugador2(), this.getIndice2()));
			this.setIndice2(pasoRosco(this.getPreguntasJugador2(), this.getIndice2()));
			muestroLetras2(this.preguntasJugador2);
			this.getVista().getLblletraQ2()
			.setText("letra: "+this.getPreguntasJugador2().get(getIndice2()).getLetra().toString());
		}
}
	@Override
	public void actionPerformed(ActionEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {


			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				
			if(juego.getJugador2()!= null) {
				if(juego.getJugador1().getEstadoPartida()||juego.getJugador2().getEstadoPartida()) {//condicion de fin de la partida entre dos jugadores
					 partidaJugadorPersonaEnter();
				}else {
					 Jugador jugadorV = juego.finJuego();
			            if (jugadorV != null) {
			                JOptionPane.showMessageDialog(
			                       getVista(), jugadorV.getNombre() + " Correctas: " + jugadorV.getContadorCorrectas()
			                                + " Incorrectas : " + jugadorV.getContadorIncorrectas(),
			                        "VENCEDOR!!!", JOptionPane.WARNING_MESSAGE);

			            } else {
			                JOptionPane.showMessageDialog(getVista(), "EMPATE!!!!!!", "NO HUBO GANADOR!!!",
			                        JOptionPane.WARNING_MESSAGE);
			           }

			            vista.dispose();
				}
				
			}else {
				if(juego.getJugador1().getEstadoPartida()||juego.getCpu().getEstadoPartida()) {//condicion de fin entre jugador y maquina
					partidaJugadorMaquinaEnter();
				}else {
         		   JOptionPane.showMessageDialog(getVista(), "Finalizo", "Bien Hecho"+ juego.getJugador1().getNombre(),
		                        JOptionPane.WARNING_MESSAGE);
         		   
         	   }
         	   vista.dispose();
			}
     
			}

			if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
				
               if(juego.getJugador2()!=null) {
            	   if(juego.getJugador1().getEstadoPartida()||juego.getJugador2().getEstadoPartida()) {
            	   partidaPersonaControl();
            	   }else {
  					 Jugador jugadorV = juego.finJuego();
			            if (jugadorV != null) {
			                JOptionPane.showMessageDialog(
			                       getVista(), jugadorV.getNombre() + " Correctas: " + jugadorV.getContadorCorrectas()
			                                + " Incorrectas : " + jugadorV.getContadorIncorrectas(),
			                        "VENCEDOR!!!", JOptionPane.WARNING_MESSAGE);

			            } else {
			                JOptionPane.showMessageDialog(getVista(), "EMPATE!!!!!!", "NO HUBO GANADOR!!!",
			                        JOptionPane.WARNING_MESSAGE);
			           }

			            vista.dispose();
            	   }
               }else {
            	   if(juego.getJugador1().getEstadoPartida()||juego.getCpu().getEstadoPartida()) {
            		   partidaJugadorMaquinaControl();
            	   }else {
            		   JOptionPane.showMessageDialog(getVista(), "Finalizo", "Bien Hecho"+ juego.getJugador1().getNombre(),
		                        JOptionPane.WARNING_MESSAGE);
            		   
            	   }
            	   vista.dispose();
               }
			}

		}



	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
