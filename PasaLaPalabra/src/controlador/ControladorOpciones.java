package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import javazoom.jlgui.basicplayer.BasicPlayerException;
import modelo.ExcepcionIngresoPalabras;
import modelo.Juego;
import modelo.Jugador;
import modelo.Maquina;
import modelo.Reproductor;
import vista.vistaOpciones;
import vista.vistaPreguntas;

public class ControladorOpciones implements ActionListener {

	private vistaOpciones vista;

	public ControladorOpciones() {
		super();

		this.vista = new vistaOpciones(this);
		this.vista.setVisible(true);

	}

	public vistaOpciones getVista() {
		return vista;
	}

	public void setVista(vistaOpciones vista) {
		this.vista = vista;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(vista.getBtnVolver())) {
			new ControladorVistaPrincipal();
			vista.dispose();
		}
		if (e.getSource().equals(vista.getBtnPreguntas())) {
			new ControladorPreguntas();
			vista.dispose();
		}
		Juego juego = new Juego();
		try {
			if (e.getSource().equals(vista.getBtnJugar())) {

				String pista = vista.getComboBoxMusica().getSelectedItem().toString();
				juego.setMusica(pista);
				System.out.println(pista);

				String tiem = vista.getComboBoxTiempo().getSelectedItem().toString();
				System.out.println(tiem);

				Integer tiempo = Integer.parseInt(tiem);
				juego.setTiempoJugador1(tiempo);

				String selcDific = vista.getComboBoxDificultad().getSelectedItem().toString();
				juego.setDificultadju(selcDific);
				System.out.println("dificultad seleccionada" + selcDific);

				juego.setDificultadju(selcDific);
				System.out.println("dificultad seleccionada" + selcDific);

				String nombre = this.getVista().getTextFieldJugador1().getText();
				if (nombre.isEmpty()) {
					throw new ExcepcionIngresoPalabras(1);
				}
				Integer puntuacion = 0;
				Integer victorias = 0;
				Integer derrotas = 0;
				Integer puntuacionTotal = 0;
				String respuesta = "";

				Jugador jugador1 = new Jugador(nombre, puntuacion, respuesta, victorias, derrotas, puntuacionTotal);

				juego.setJugador1(jugador1);

				if (vista.getRdbtnJugador().isSelected()) {
					System.out.println("Seleccionado jugador 2");
					nombre = this.getVista().getTextFieldJugador2().getText();
					if (nombre.isEmpty()) {
						throw new ExcepcionIngresoPalabras(1);
					}
					if(nombre.equals(jugador1.getNombre())) {
						throw new ExcepcionIngresoPalabras(2);
					}
					Jugador jugador2 = new Jugador(nombre, puntuacion, respuesta, victorias, derrotas, puntuacionTotal);

					juego.setJugador2(jugador2);
					this.getVista().getBtnJugar().setEnabled(true);

				} else if (vista.getRdbtnCpu().isSelected()) {

					System.out.println("Seleccionado maquina");
					String nombreCpu = this.getVista().getTextFieldJugador2().getText();
					String dificultad = (String) this.getVista().getComboBoxDificultad().getSelectedItem();
                    
					Maquina cpu = null;

					cpu = new Maquina(nombreCpu, puntuacionTotal, respuesta,dificultad);
					juego.setJugador2(null);
					juego.setCpu(cpu);
					this.getVista().getBtnJugar().setEnabled(true);

				}

				new ControladorJuego(juego);
				vista.dispose();
			}
		} catch (ExcepcionIngresoPalabras e1) {
			JOptionPane.showMessageDialog(getVista(), e1.getMessage());
			e1.getMessage();
			this.getVista().getLblIngreseNombre().setVisible(true);
			this.getVista().getLblIngreseNombre_1().setVisible(true);
		}
	}
}
