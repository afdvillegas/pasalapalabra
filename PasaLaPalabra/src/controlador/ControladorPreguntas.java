package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.print.attribute.standard.JobKOctetsProcessed;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.postgresql.util.PSQLException;

import modelo.ExcepcionIngresoPalabras;
import modelo.ExcepcionPregunta;
import modelo.Pregunta;
import modelo.PreguntaDao;
import vista.vistaPreguntaModificar;
import vista.vistaPreguntas;

public class ControladorPreguntas implements ActionListener {

	private vistaPreguntas vista;
	private PreguntaDao preguntas;

	public ControladorPreguntas() {

		preguntas = new PreguntaDao();
		this.vista = new vistaPreguntas(this);
		this.vista.setVisible(true);
		actualizarModeloTabla();
	}

	private void actualizarModeloTabla() {
		DefaultTableModel modelotabla = new DefaultTableModel();
		modelotabla.addColumn("ID");
		modelotabla.addColumn("Pregunta");
		modelotabla.addColumn("Respuesta");

		for (Pregunta pregunta : getPreguntas().getAll()) {
			Vector<String> vector = new Vector<String>();
			vector.add(pregunta.getId_pregunta().toString());
			vector.add(pregunta.getPregunta());
			vector.add(pregunta.getRespuesta());
			modelotabla.addRow(vector);

		}
		getVista().getTable().setModel(modelotabla);
		getVista().getTable().getColumnModel().getColumn(0).setMaxWidth(0);
		getVista().getTable().getColumnModel().getColumn(0).setMinWidth(0);
		getVista().getTable().getColumnModel().getColumn(0).setPreferredWidth(0);
	}

	public PreguntaDao getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(PreguntaDao preguntas) {
		this.preguntas = preguntas;
	}

	public vistaPreguntas getVista() {
		return vista;
	}

	public void setVista(vistaPreguntas vista) {
		this.vista = vista;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(vista.getBtnvolver())) {
			new ControladorOpciones();
			vista.dispose();

		}

		if (e.getSource().equals(vista.getBtnAgregar())) {
			new ControladorPreguntasNueva();
			vista.dispose();
		}
		if (e.getSource().equals(vista.getBtnReporte())) {
			new ControladorReportePreguntas();
			vista.dispose();
		}
		try {

			if (e.getSource().equals(vista.getBtnEliminar())) {

				Pregunta t;
				try {
					t = new Pregunta(null, "p", "p");

					String id = (String) this.getVista().getTable()
							.getValueAt(this.getVista().getTable().getSelectedRow(), 0);

					t.setId_pregunta(Integer.parseInt(id));

					System.out.println(
							this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 0));

					this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 0);

					int valor = JOptionPane.showConfirmDialog(getVista(), "�Esta seguro de borrar?");
					if (valor == JOptionPane.YES_OPTION) {
						getPreguntas().delete(t);
						this.getVista().getTable().repaint();
						JOptionPane.showMessageDialog(getVista(), "Pregunta eliminada");
					}

				} catch (ExcepcionIngresoPalabras e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} catch (ArrayIndexOutOfBoundsException e1) {
			JOptionPane.showMessageDialog(getVista(), "Selecciona una pregunta para eliminar");
		}

		try {

			if (e.getSource().equals(vista.getBtnModificar())) {
				// Pregunta t = new Pregunta(null, "p", "p");
				String id = (String) this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(),
						0);
				String pregunta = (String) this.getVista().getTable()
						.getValueAt(this.getVista().getTable().getSelectedRow(), 1);
				String respuesta = (String) this.getVista().getTable()
						.getValueAt(this.getVista().getTable().getSelectedRow(), 2);
				Pregunta t;
				try {

					t = new Pregunta(Integer.parseInt(id), pregunta, respuesta);
					if (t.comprobarSiHayNumeros(t.getPregunta())) {
						throw new ExcepcionIngresoPalabras(3);
					}

					if (t.comprobarSiHayNumeros(t.getPregunta())) {
						throw new ExcepcionIngresoPalabras(3);
					}
					new ControladorPreguntasModificar(t);
				} catch (NumberFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ExcepcionIngresoPalabras e1) {
					JOptionPane.showMessageDialog(getVista(), e1.getMessage());
				}

				vista.dispose();
			}
		} catch (ArrayIndexOutOfBoundsException e1) {
			JOptionPane.showMessageDialog(getVista(), "Selecciona una pregunta para modificar");
		}
	}
}
