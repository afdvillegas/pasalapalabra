package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import modelo.ExcepcionIngresoPalabras;
import modelo.ExcepcionPregunta;
import modelo.Pregunta;
import modelo.PreguntaDao;
import vista.vistaPreguntaModificar;

public class ControladorPreguntasModificar implements ActionListener {
	private vistaPreguntaModificar vista;
	private Pregunta preguntaModificar;
	private PreguntaDao preguntas;
	private ArrayList<Pregunta> listaPregunta = new ArrayList<Pregunta>();

	public ControladorPreguntasModificar(Pregunta pregunta) {
		super();
		preguntas = new PreguntaDao();
		this.vista = new vistaPreguntaModificar(this);
		this.vista.setVisible(true);
		this.getVista().getTextFieldPregunta().setText(pregunta.getPregunta());
		this.getVista().getTextFieldRespuesta().setText(pregunta.getRespuesta());
		this.setPreguntaModificar(pregunta);

	}

	public vistaPreguntaModificar getVista() {
		return vista;
	}

	public void setVista(vistaPreguntaModificar vista) {
		this.vista = vista;
	}

	public PreguntaDao getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(PreguntaDao preguntas) {
		this.preguntas = preguntas;
	}

	public Pregunta getPreguntaModificar() {
		return preguntaModificar;
	}

	public void setPreguntaModificar(Pregunta preguntaModificar) {
		this.preguntaModificar = preguntaModificar;
	}

	public ArrayList<Pregunta> getListaPregunta() {
		return listaPregunta;
	}

	public void setListaPregunta(ArrayList<Pregunta> listaPregunta) {
		this.listaPregunta = listaPregunta;
	}

	public void tengopreguntas(Pregunta pregunta) throws ExcepcionPregunta {
		// Excepcion para respuesta repetidas o respuestas con letras que no hay letra
		// en el rosco

		listaPregunta = (ArrayList<Pregunta>) preguntas.getAll();
		Integer contador = 0;
		for (Pregunta pregunta1 : listaPregunta) {

			if (pregunta1.getRespuesta().equals(pregunta.getRespuesta())) {
				if (contador == 3) {
					throw new ExcepcionPregunta(50);
				}
				contador++;

			} 

		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(vista.getBtnModificar())) {
			try {

				this.getPreguntaModificar().setPregunta(this.getVista().getTextFieldPregunta().getText());

				this.getPreguntaModificar()
						.setRespuesta(this.getVista().getTextFieldRespuesta().getText().toUpperCase());
				this.getPreguntaModificar().getId_pregunta();

				Pregunta preg = new Pregunta(this.getPreguntaModificar().getId_pregunta(),
						this.getPreguntaModificar().getPregunta(), this.getPreguntaModificar().getRespuesta());

				tengopreguntas(preg);
				if (this.getPreguntaModificar().comprobarSiHayNumeros(this.getPreguntaModificar().getPregunta())) {
					throw new ExcepcionIngresoPalabras(3);
				}
				if (this.getPreguntaModificar().comprobarSiHayNumeros(this.getPreguntaModificar().getRespuesta())) {
					throw new ExcepcionIngresoPalabras(3);
				}
				
				getPreguntas().update(this.getPreguntaModificar());
			
			} catch (ExcepcionPregunta e1) {
				JOptionPane.showMessageDialog(getVista(), e1.getMessage(), "OJO!!!", JOptionPane.ERROR_MESSAGE);
			} catch (ExcepcionIngresoPalabras e1) {
				JOptionPane.showMessageDialog(getVista(), e1.getMessage());
			}

		}
		if (e.getSource().equals(vista.getBtnVolver())) {
			new ControladorPreguntas();
			vista.dispose();
		}
	}

}
