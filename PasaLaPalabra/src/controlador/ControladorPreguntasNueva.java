package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import javax.swing.JOptionPane;

import javax.swing.JOptionPane;

import javax.swing.JOptionPane;

import modelo.ExcepcionIngresoPalabras;
import modelo.ExcepcionPregunta;

import modelo.Pregunta;
import modelo.PreguntaDao;
import vista.vistaPreguntasNueva;

public class ControladorPreguntasNueva implements ActionListener {

	private vistaPreguntasNueva vista;
	private PreguntaDao preguntas;
	private ArrayList<Pregunta> listaPregunta = new ArrayList<Pregunta>();

	public ControladorPreguntasNueva() {
		this.preguntas = new PreguntaDao();
		this.vista = new vistaPreguntasNueva(this);
		this.vista.setVisible(true);
		// habilitarBotones();
	}

	public PreguntaDao getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(PreguntaDao preguntas) {
		this.preguntas = preguntas;
	}

	public vistaPreguntasNueva getVista() {
		return vista;
	}

	public void setVista(vistaPreguntasNueva vista) {
		this.vista = vista;
	}

	public ArrayList<Pregunta> getListaPregunta() {
		return listaPregunta;
	}

	public void setListaPregunta(ArrayList<Pregunta> listaPregunta) {
		this.listaPregunta = listaPregunta;
	}

	public void tengopreguntas(Pregunta pregunta) throws ExcepcionPregunta {
		// Excepcion para respuesta repetidas o respuestas con letras que no hay letra
		// en el rosco

		listaPregunta = (ArrayList<Pregunta>) preguntas.getAll();

		for (Pregunta pregunta1 : listaPregunta) {

			if (pregunta1.getRespuesta().equals(pregunta.getRespuesta())) {
				throw new ExcepcionPregunta(50);

			}

		}

	}

	public void habilitarBotones() {
		if (this.getVista().getRdbtnContiene().isSelected()) {
			this.getVista().getComboBoxLetra().setEnabled(true);
		}

	}

	public void verificarLetra(Pregunta pregunta) throws ExcepcionIngresoPalabras {

		if (this.getVista().getRdbtnContiene().isSelected()) {
			Character letraABuscar = (Character) this.getVista().getComboBoxLetra().getSelectedItem();

			for (int i = 0; i < pregunta.getRespuesta().length(); i++) {
				Character letraPregunta = pregunta.getRespuesta().charAt(i);
				letraPregunta = Character.toUpperCase(letraPregunta);
				if (letraPregunta == letraABuscar) {
					pregunta.setLetra(letraABuscar);
				}
			}
			
			if (pregunta.getLetra() == letraABuscar) {
				String preguntaNueva = pregunta.getPregunta();

				System.out.println("Se encuentra la letra y se asigna a la pregunta");
				pregunta.setPregunta("Contiene " + letraABuscar + ": " + preguntaNueva);
			} else {
				System.out.println("No se encontro la letra buscada");
				pregunta.setLetra(pregunta.getRespuesta().charAt(0));
			}

		}else {
			pregunta.setLetra(pregunta.getRespuesta().charAt(0));
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(vista.getBtnAgregar())) {

			Integer id = getPreguntas().maxId() + 1;
			String preguntaa = getVista().getTextFieldPregunta().getText();

			String respuesta = getVista().getTextFieldRespuesta().getText().toUpperCase();

			try {

				Pregunta preg = new Pregunta(id, preguntaa, respuesta);

				if (preg.comprobarSiHayNumeros(preg.getPregunta())) {
					throw new ExcepcionIngresoPalabras(3);
				}
				if (preg.comprobarSiHayNumeros(preg.getRespuesta())) {
					throw new ExcepcionIngresoPalabras(3);
				}
				if(preg.getPregunta().isEmpty()) {
					throw new ExcepcionIngresoPalabras(1);
				}
				if(preg.getRespuesta().isEmpty()) {
					throw new ExcepcionIngresoPalabras(1);
				}
				
				verificarLetra(preg);

				tengopreguntas(preg);

				getPreguntas().save(preg);

				new ControladorPreguntas();
				vista.dispose();

			} catch (ExcepcionIngresoPalabras e2) {
				JOptionPane.showMessageDialog(getVista(), e2.getMessage());
			} catch (ExcepcionPregunta e2) {
				JOptionPane.showMessageDialog(getVista(), e2.getMessage());
			}catch (StringIndexOutOfBoundsException e2) {
				JOptionPane.showMessageDialog(getVista(), "Error no ingresa respuesta");
			}

		}

		if (e.getSource().equals(vista.getBtnCancelar())) {
			new ControladorPreguntas();
			vista.dispose();
		}
	}
}