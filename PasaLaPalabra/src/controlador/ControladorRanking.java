package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import modelo.Jugador;
import modelo.RankingDAO;
import modelo.Reportes;
import modelo.Pregunta;
import vista.vistaRanking;

public class ControladorRanking implements ActionListener {

	private vistaRanking vista;
	private RankingDAO jugadores;

	public ControladorRanking() {
		super();
		jugadores = new RankingDAO();
		this.vista = new vistaRanking(this);
		this.vista.setVisible(true);
		actualizarModeloTabla();
	}

	private void actualizarModeloTabla() {
		DefaultTableModel modelotabla = new DefaultTableModel();
		modelotabla.addColumn("Nombre");
		modelotabla.addColumn("Victorias");
		modelotabla.addColumn("Derrotas");
		modelotabla.addColumn("Puntuacion");

		for (Jugador jugador : getJugadores().getAll()) {
			Vector<String> vector = new Vector<String>();

			vector.add(jugador.getNombre());
			vector.add(jugador.getVictorias().toString());
			vector.add(jugador.getDerrotas().toString());
			vector.add(jugador.getPuntuacionTotal().toString());
			modelotabla.addRow(vector);
		}
		getVista().getTable().setModel(modelotabla);
		TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>(modelotabla);
		getVista().getTable().setRowSorter(sorter);

	}

	public vistaRanking getVista() {
		return vista;
	}

	public void setVista(vistaRanking vista) {
		this.vista = vista;
	}

	public RankingDAO getJugadores() {
		return jugadores;
	}

	public void setJugadores(RankingDAO jugadores) {
		this.jugadores = jugadores;
	}

	@Override
	public void actionPerformed(ActionEvent e) {


		if(e.getSource().equals(vista.getBtnvolver())) {
			new ControladorVistaPrincipal();
			vista.dispose();
		}
		if(e.getSource().equals(vista.getBtnTopJugadores())) {
			try {
				Reportes.reporteMejoresJugadores();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if(e.getSource().equals(vista.getBtnReporte())) {
			try {
				Reportes.reportesJugadores();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}
}
