package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Reportes;
import vista.vistaReportePreguntas;

public class ControladorReportePreguntas implements ActionListener {

	private vistaReportePreguntas vista;

	public ControladorReportePreguntas() {
		super();
		this.vista = new vistaReportePreguntas(this);
		this.vista.setVisible(true);
	}

	public vistaReportePreguntas getVista() {
		return vista;
	}

	public void setVista(vistaReportePreguntas vista) {
		this.vista = vista;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(vista.getBtnGenerar())) {
			Character letra = (Character) this.getVista().getComboBoxLetra().getSelectedItem();
			Reportes.reportePreguntasPorLetra(letra);
			vista.dispose();
			new ControladorPreguntas();
		}
	}

}
