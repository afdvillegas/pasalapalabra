package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Juego;
import modelo.Jugador;
import vista.vistaPrincipal;

public class ControladorVistaPrincipal implements ActionListener {

	private vistaPrincipal vista;

	public ControladorVistaPrincipal() {
		super();
		this.vista = new vistaPrincipal(this);
		this.vista.setVisible(true);
	}

	public vistaPrincipal getVista() {
		return vista;
	}

	public void setVista(vistaPrincipal vista) {
		this.vista = vista;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(vista.getBtnSalir())) {
			vista.setVisible(false);
		}
		if (e.getSource().equals(vista.getBtnOpcionesDeJuego())) {
			new ControladorOpciones();
			vista.dispose();
		}
		if (e.getSource().equals(vista.getBtnRanking())) {
			new ControladorRanking();
			vista.dispose();

		}

	}
}
