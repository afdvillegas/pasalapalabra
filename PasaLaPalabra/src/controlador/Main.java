package controlador;

import java.awt.EventQueue;

import vista.vistaPrincipal;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorVistaPrincipal();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
