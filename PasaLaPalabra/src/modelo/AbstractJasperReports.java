package modelo;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.HashMap;

import com.mysql.fabric.xmlrpc.base.Data;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class AbstractJasperReports {

	private static JasperReport report;
	private static JasperPrint reportFilled;

	public static void createReport(ResultSet info, String path) {
		HashMap<String, Object> parametros = new HashMap<String, Object>();

		try {
			
			report = (JasperReport) JRLoader.loadObjectFromFile(path);
			reportFilled = JasperFillManager.fillReport(report, parametros, new JRResultSetDataSource(info));
		

		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	public static void createReportParametro(ResultSet info, String path,HashMap<String, Object>parametros) {
		//HashMap<String, Object> parametros = new HashMap<String, Object>();

		try {
			
			report = (JasperReport) JRLoader.loadObjectFromFile(path);
			reportFilled = JasperFillManager.fillReport(report, parametros, new JRResultSetDataSource(info));
		

		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	public static void exportToPDF(String destination) {
		try {
			JasperExportManager.exportReportToPdfFile(reportFilled, destination); // convierte a pdf
		} catch (JRException ex) {
			ex.printStackTrace();
		}
		try {
			File path = new File(destination);
			Desktop.getDesktop().open(path);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
