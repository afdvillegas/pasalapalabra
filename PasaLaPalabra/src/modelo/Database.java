package modelo;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.crypto.Data;

public class Database {

	private Connection conn;
	private Connection conexion;
	private static Database db = null;
	private final String USER = "postgres";

	private final String PSW = "postgres";
	private final String DBNAME = "pasapalabra";


	private final String PORT = "5432";
	private final String HOST = "localhost";

	private Database() {
		try {
			this.setConn(
					DriverManager.getConnection("jdbc:postgresql://" + HOST + ":" + PORT + "/" + DBNAME, USER, PSW));
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	public static Database getInstance() {
		return db != null ? db : new Database();
	}

	public ResultSet query(String query) {
		ResultSet rs = null;
		try {
			rs = getConn().createStatement().executeQuery(query);
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("Realizada la consulta");
		}
		return rs;
	}

	private Connection getConn() {
		return conn;
	}

	private void setConn(Connection conn) {
		this.conn = conn;
	}

	public Connection getConexion() {
		return conexion;
	}

	public void setConexion(Connection conexion) {
		this.conexion = conexion;
	}
	

}
