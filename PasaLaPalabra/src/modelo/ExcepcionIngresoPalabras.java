package modelo;

public class ExcepcionIngresoPalabras extends Exception {

	private Integer code;
	
	

	public ExcepcionIngresoPalabras(Integer code) {
		super();
		this.setCode(code);
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		String mensaje = "";
		
		if(this.getCode()==1) {
			mensaje = "No se puede ingresar espacio vacio";
		}
		if(this.getCode()==2) {
			mensaje = "No pueden tener el mismo nombre";
		}
		if(this.getCode()==3) {
			mensaje=  "no se pueden asignar numeros a la pregunta";
		}
		return mensaje;
		
	}
	
	
	
}
