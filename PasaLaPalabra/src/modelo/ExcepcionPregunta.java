package modelo;

public class ExcepcionPregunta extends Exception {

	private Integer code;



	public ExcepcionPregunta(Integer code) {
		super();
		this.setCode(code);
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		String mensaje = "";
		if(this.getCode() ==50) {
			mensaje = "YA EXISTEN MUCHAS PREGUNTAS PARA ESA RESPUESTA";
		}
		return mensaje;
	}

}
