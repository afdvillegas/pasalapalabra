package modelo;

public class ExcepcionProblemasRosco extends Exception {

	Integer code;

	public ExcepcionProblemasRosco(Integer code) {
		super();
		this.setCode(code);
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		String mensaje = "";

		if (this.getCode() == 1) {
			mensaje = "Faltan preguntas en el rosco, no se pudo llenar";
		}
		if(this.getCode() ==2) {
			mensaje = "Faltan letras alfabeticas en el rosco";
		}
		return mensaje;
	}

}
