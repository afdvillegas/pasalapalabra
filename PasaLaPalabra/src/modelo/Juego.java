package modelo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

import javax.swing.JOptionPane;
import javax.swing.text.StyledEditorKit.ForegroundAction;
import javax.swing.text.html.HTMLDocument.Iterator;

import org.omg.CORBA.OMGVMCID;

public class Juego {

	private Jugador jugador1;
	private Jugador jugador2;
	private Maquina cpu;
	private Integer tiempoJugador1;
	private Integer tiempoJugador2;
	private String musica;
	private PreguntaDao preguntas;
	private String dificultadju;
	private RankingDAO ranking;

	public Juego() {
		super();

		this.setJugador1(jugador1);
		this.setJugador2(jugador2);
		this.setCpu(cpu);
		this.setTiempoJugador1(tiempoJugador1);
		this.setTiempoJugador2(tiempoJugador2);
		this.setMusica(musica);
		this.setDificultadju(dificultadju);
		this.preguntas = new PreguntaDao();
		this.ranking = new RankingDAO();

	}

	public Jugador getJugador1() {
		return jugador1;
	}

	public void setJugador1(Jugador jugador1) {
		this.jugador1 = jugador1;
	}

	public Jugador getJugador2() {
		return jugador2;
	}

	public void setJugador2(Jugador jugador2) {
		this.jugador2 = jugador2;
	}

	public Maquina getCpu() {
		return cpu;
	}

	public void setCpu(Maquina cpu) {
		this.cpu = cpu;
	}

	public Integer getTiempoJugador1() {
		return tiempoJugador1;
	}

	public void setTiempoJugador1(Integer tiempoJugador1) {
		this.tiempoJugador1 = tiempoJugador1;
	}

	public Integer getTiempoJugador2() {
		return tiempoJugador2;
	}

	public void setTiempoJugador2(Integer tiempoJugador2) {
		this.tiempoJugador2 = tiempoJugador2;
	}

	public String getMusica() {
		return musica;
	}

	public void setMusica(String musica) {
		this.musica = musica;
	}

	public PreguntaDao getPreguntas() {
		return preguntas;
	}

	public void setPreguntas(PreguntaDao preguntas) {
		this.preguntas = preguntas;
	}

	public String getDificultadju() {
		return dificultadju;
	}

	public void setDificultadju(String dificultadju) {
		this.dificultadju = dificultadju;
	}

	public RankingDAO getRanking() {
		return ranking;
	}

	public void setRanking(RankingDAO ranking) {
		this.ranking = ranking;
	}

	// Este metodo verifica si estan todas las letras del alfabeto en el array
	private boolean comprobarTodasLetras(ArrayList<Pregunta> preguntas) {

		boolean[] arrayLetras = new boolean[26];
		char letra;
		int index;
		for (int i = 0; i < preguntas.size(); i++) {
			letra = preguntas.get(i).getLetra();
			if ('A' <= letra && letra <= 'Z') {
				index = letra - 'A';
				arrayLetras[index] = true;
			}
		}

		for (int i = 0; i < arrayLetras.length; i++) {
			if (!arrayLetras[i]) {
				System.out.println("No tiene todas las letras");
				return false;
			}
		}
		System.out.println("Tiene todas las letras");
		return true;
	}
//se obtienen las pregunta para cada jugador
	public ArrayList<Pregunta> obtengopreguntas() throws ExcepcionProblemasRosco {
        ArrayList<Pregunta> preguntas = (ArrayList<Pregunta>) getPreguntas().getAll();
        Collections.shuffle(preguntas);

        try {

            for (int i = 0; i < preguntas.size(); i++) {

                for (int j = 0; j < preguntas.size(); j++) {

                    if (i != j) {
                        if (preguntas.get(j).getLetra().equals(preguntas.get(i).getLetra())) {
                            preguntas.remove(j);

                        }
                    }
                }
            }

            if(preguntas.size()!=26) {
                throw new ExcepcionProblemasRosco(1);
            }
        }catch (IndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "Error al cargar rosco, intente de nuevo");
        }

            // Metodo para ordenar arreglo hecho en la clase Pregunta
            Collections.sort(preguntas);





        if (comprobarTodasLetras(preguntas) == false) {
            try {
                throw new ExcepcionProblemasRosco(2);
            } catch (ExcepcionProblemasRosco e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		return preguntas;
	}
	public Jugador finJuego() {
		// cuando termina la partida se evalua al ganador
		Integer v = 0;
		Integer d = 0;
		Jugador ganador;
		Jugador perdedor;

		if (getJugador1().getContadorCorrectas() > getJugador2().getContadorCorrectas()) {

			getJugador1().setVictorias(v + 1);
			getJugador2().setDerrotas(d + 1);

			// Ganador que existe en la BDD
			if (ranking.get(getJugador1().getNombre()) != null) {

				ganador = ranking.get(getJugador1().getNombre());
				ganador.setVictorias(ganador.getVictorias() + 1);
				ganador.setPuntuacionTotal(ganador.getPuntuacionTotal() + getJugador1().getContadorCorrectas());

				//Se modifica el ganador
				ranking.update(ganador);	

			} else {

				// Ganador que no existe en la BDD
				getJugador1().setVictorias(1);
				getJugador1().setPuntuacionTotal(getJugador1().getContadorCorrectas());
				ranking.save(getJugador1());
				System.out.println("Se guarda" + getJugador1().getNombre());
			}
			
			//Perdedor que existe enla base de datos
			if (ranking.get(getJugador2().getNombre()) != null) {
				perdedor = ranking.get(getJugador2().getNombre());
				perdedor.setDerrotas(perdedor.getDerrotas() + 1);
				ranking.update(perdedor);
			} else {
				//Perdedor que no existe en la base de datos
				getJugador2().setDerrotas(1);
				ranking.save(getJugador2());
			}

			return getJugador1();
		} else if (getJugador1().getContadorCorrectas() < getJugador2().getContadorCorrectas()) {
			getJugador2().setVictorias(v + 1);
			getJugador1().setDerrotas(d + 1);

			if (ranking.get(getJugador2().getNombre()) != null) {
				// Ganador que existe en la BDD
				ganador = ranking.get(getJugador2().getNombre());
				ganador.setVictorias(ganador.getVictorias() + 1);
				ganador.setPuntuacionTotal(ganador.getPuntuacionTotal() + getJugador2().getContadorCorrectas());

				ranking.update(ganador);
				System.out.println("Se modifica");



			} else {

				// Ganador que no existe en la BDD
				getJugador2().setVictorias(1);
				getJugador2().setPuntuacionTotal(getJugador2().getContadorCorrectas());
				ranking.save(getJugador2());
				System.out.println("Se guarda" + getJugador2().getNombre());
			}
			
			if (ranking.get(getJugador1().getNombre()) != null) {
				perdedor = ranking.get(getJugador1().getNombre());
				perdedor.setDerrotas(perdedor.getDerrotas() + 1);
				ranking.update(perdedor);
			} else {
				getJugador1().setDerrotas(1);
				ranking.save(getJugador1());
			}

			return getJugador2();
		}
		return null;

	}

}
