package modelo;

import java.util.ArrayList;
import java.util.Scanner;

public class Jugador extends Player {

	private Integer victorias;
	private Integer derrotas;
	private Integer puntuacionTotal;
	private Integer contadorCorrectas;
	private Integer contadorIncorrectas;

	public Jugador(String nombre, Integer puntuacion, String respuesta, Integer victorias, Integer derrotas,
			Integer puntuacionTotal) {
		super(nombre, puntuacion, respuesta);
		this.setVictorias(victorias);
		this.setDerrotas(derrotas);
		this.setPuntuacionTotal(puntuacionTotal);
		this.setEstadoPartida(true);
		this.setContadorCorrectas(0);
		this.setContadorIncorrectas(0);
	}

	public Integer getVictorias() {
		return victorias;
	}

	public void setVictorias(Integer victorias) {
		this.victorias = victorias;
	}

	public Integer getDerrotas() {
		return derrotas;
	}

	public void setDerrotas(Integer derrotas) {
		this.derrotas = derrotas;
	}

	public Integer getPuntuacionTotal() {
		return puntuacionTotal;
	}

	public void setPuntuacionTotal(Integer puntuacionTotal) {
		this.puntuacionTotal = puntuacionTotal;
	}

	public Integer getContadorCorrectas() {
		return contadorCorrectas;
	}

	public void setContadorCorrectas(Integer contadorCorrectas) {
		this.contadorCorrectas = contadorCorrectas;
	}

	public Integer getContadorIncorrectas() {
		return contadorIncorrectas;
	}

	public void setContadorIncorrectas(Integer contadorIncorrectas) {
		this.contadorIncorrectas = contadorIncorrectas;
	}

	@Override
	public void responder(Pregunta pregunta, String respuesta) {
//metodo heredado de player  y jugador lo aplica como quiere
		if (pregunta.getEstadoP().equals("sin responder") || pregunta.getEstadoP().equals("paso")) {

			if (pregunta.getRespuesta().equals(respuesta.toUpperCase())) {
				pregunta.setEstadoP("correcto");
				setEstadoJugador(true);

			} else {
				pregunta.setEstadoP("incorrecto");
				System.out.println("estadoo " + pregunta.getEstadoP());
				setEstadoJugador(false);
			}

		}

	}
//evalua en que momento el jugador ya termino
	public Boolean terminaJugador(Integer tiempo, ArrayList<Pregunta> listacontrol) {
		Boolean termina = true;
		Integer contadorCorrectas = 0;
		Integer contadorIncorrectas = 0;
		Integer total = 0;

		for (Pregunta pregunta : listacontrol) {
			if (pregunta.getEstadoP() != null) {

				if (pregunta.getEstadoP().equals("correcto")) {
					contadorCorrectas++;
					System.out.println("correctas" + getContadorCorrectas());
				} else if (pregunta.getEstadoP().equals("incorrecto")) {
					contadorIncorrectas++;
					System.out.println("incorrectas:" + getContadorIncorrectas());
				}
			}
		}

		if (tiempo > 0) {

			total = contadorCorrectas + contadorIncorrectas;
			if (total.equals(26)) {
				System.out.println("Termina el juego, no hay mas preguntas");
				System.out.println("termine");
				setContadorCorrectas(contadorCorrectas);
				setContadorIncorrectas(contadorIncorrectas);
				return false;
			}

		} else if (tiempo <= 0) {
			System.out.println("termine por tiempo");
			setContadorCorrectas(contadorCorrectas);
			setContadorIncorrectas(contadorIncorrectas);
			termina = false;
		}

		return termina;

	}
}