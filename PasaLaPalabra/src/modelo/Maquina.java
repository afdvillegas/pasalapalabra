package modelo;

import java.util.ArrayList;
import java.util.Random;

public class Maquina extends Player {

	
	private String dificultad;


	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public Maquina(String nombre, Integer puntuacion, String respuesta, String dificultad) {
		super(nombre, puntuacion, respuesta);

		setDificultad(dificultad);
        setEstadoPartida(true);
	}
// evalua a la maquina para terminar
	public Boolean maquinaTermina(ArrayList<Pregunta> listacontrol, Integer tiempo) {
		Boolean termina = true;
		Integer contador = 0;

		for (Pregunta pregunta : listacontrol) {
			if (pregunta.getEstadoP() != null) {

				if (pregunta.getEstadoP().equals("correcto") || pregunta.getEstadoP().equals("incorrecto")) {
					contador++;
				}
			}
		}
		if (contador == 26 || tiempo <= 0) {
			termina = false;
		}

		return termina;

	}
//metodo heredado de playar y maquina responde mediante probabilidades 
	
	@Override
	public void responder(Pregunta pregunta, String respuesta) {
		Random r = new Random();
		
		
	if(pregunta.getEstadoP().equals("sin responder")|| pregunta.getEstadoP().equals("paso")) {
		
		
		switch (this.getDificultad()) {
		case "Facil":
			try {
				Thread.sleep(3000);
				System.out.println("pregunta: " + pregunta.getPregunta() + pregunta.getRespuesta());
				if (r.nextInt(5) >2) {
					System.out.println("respondo");

					if (r.nextInt(3) > 1) {
						pregunta.setEstadoP("correcto");
						this.setDificultad("Normal");//se settea la dificultad para evitar que la proxima pregunta la responda de la misma manera
						  this.setEstadoJugador(true);
					} else
						System.out.println("Respondo Mal estado mal");
					pregunta.setEstadoP("incorrecto");
					this.setDificultad("Facil");
					  this.setEstadoJugador(false);
				} else {
					pregunta.setEstadoP("paso");
					  this.setEstadoJugador(false);
				}
					System.out.println("Paso No Respondo mal");
				
				
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			
			break;
case "Normal":
			try {
				Thread.sleep(2000);
				System.out.println("pregunta: " + pregunta.getPregunta() + pregunta.getRespuesta());
				if (r.nextInt(4)>2) {
					
					System.out.println(" Voy a Responder: ");
					if(r.nextInt(2)>1) {
						System.out.println("Correcto  estado normal" );
						pregunta.setEstadoP("correcto");
						this.setDificultad("Dificil");
						  this.setEstadoJugador(true);
						
					}else { System.out.println("Respondo Mal estado normal");
					
					pregunta.setEstadoP("incorrecto");
					this.setDificultad("Facil");
					  this.setEstadoJugador(false);
					}
				}else {
					System.out.println("Paso no respondo normal");
					pregunta.setEstadoP("paso");
					  this.setEstadoJugador(false);
				}
				
				
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	
	


			break;
case "Dificil":
			try {
				Thread.sleep(1000);
				System.out.println("pregunta: " + pregunta.getPregunta() + pregunta.getRespuesta());
				if(r.nextInt(3)>1) {
					System.out.println("respondo");
					
					System.out.println("Correcto estado bueno ");
					pregunta.setEstadoP("correcto");
					this.setDificultad("Dificil");
					  this.setEstadoJugador(true);
				}else {
					pregunta.setEstadoP("paso");
					this.setEstadoJugador(false);
				}
				
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		
	
	
	break;

		default:System.out.println("no se respondiooo");
		System.out.println("dificultad"+ getDificultad() );
			break;
		}
	}
	}

	
}
