package modelo;
//clase abs que los hijos , jugador y maquina utilizan
public abstract class Player {
	private String nombre;
	private Integer puntuacion;
	private String respuesta;
    private Boolean estadoJugador;
    private Boolean estadoPartida;
	public Player(String nombre, Integer puntuacion, String respuesta) {
		super();
		this.setNombre(nombre);
		this.setPuntuacion(puntuacion);
		this.setRespuesta(respuesta);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(Integer puntuacion) {
		this.puntuacion = puntuacion;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}

	public Boolean getEstadoJugador() {
		return estadoJugador;
	}

	public void setEstadoJugador(Boolean estadoJugador) {
		this.estadoJugador = estadoJugador;
	}

	public Boolean getEstadoPartida() {
		return estadoPartida;
	}

	public void setEstadoPartida(Boolean estadoPartida) {
		this.estadoPartida = estadoPartida;
	}

	public abstract void responder(Pregunta pregunta,String respuesta) ;

	
	


}
