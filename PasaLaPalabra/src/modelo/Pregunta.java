package modelo;

public class Pregunta implements Comparable<Pregunta> {

	private Integer id_pregunta;
	private String pregunta;
	private String respuesta;
	private Character letra;
	private String estadoP;

	public Pregunta(Integer id_pregunta, String pregunta, String respuesta) throws ExcepcionIngresoPalabras {
		super();
		this.setId_pregunta(id_pregunta);
		this.setPregunta(pregunta);
		this.setRespuesta(respuesta);
		this.setLetra(respuesta.charAt(0));
		this.setEstadoP("sin responder");
		
	}
	
	

	public String getEstadoP() {
		return estadoP;
	}



	public void setEstadoP(String estadoP) {
		this.estadoP = estadoP;
	}



	public Character getLetra() {
		return letra;
	}







	public void setLetra(Character letra) {
		this.letra = letra;
	}



	public Integer getId_pregunta() {
		return id_pregunta;
	}

	public void setId_pregunta(Integer id_pregunta) {
		this.id_pregunta = id_pregunta;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) throws ExcepcionIngresoPalabras {
		this.pregunta = pregunta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	public Boolean comprobarSiHayNumeros(String texto) {

		for (int i = 0; i < texto.length(); i++) {
			if (Character.isDigit(texto.charAt(i))) {
				return true;
			}
		}
		return false;

	}

//Se imaplementa Comparable para poder ordenar arreglos, en este caso se ordena por la letra
	@Override
	public int compareTo(Pregunta o) {
		if(o.getLetra()>getLetra()) {
			return -1;
		}
		if(o.getLetra()<getLetra()) {
			return 1;
		}
		return 0;
	}
}
