package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.swing.JOptionPane;

public class PreguntaDao implements Dao<Pregunta> {

	private Database db = Database.getInstance();

	@Override
	public List<Pregunta> getAll() {
		List<Pregunta> preguntas = new ArrayList<Pregunta>();
		ResultSet rs = getDb().query("SELECT * FROM pregunta");
		try {
			while (rs.next()) {
				Integer id = rs.getInt(1);
				String preguntaTexto = rs.getString(2);
				String respuestaTexto = rs.getString(3);
				Pregunta pregunta;
				try {
					pregunta = new Pregunta(id, preguntaTexto, respuestaTexto);
					preguntas.add(pregunta);
				} catch (ExcepcionIngresoPalabras e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return preguntas;
	}

	@Override
	public Optional<Pregunta> get(Integer id) {
		Pregunta pregunta = null;
		ResultSet rs = getDb().query("SELECT * FROM pregunta WHERE pregunta.id= " + id + "LIMIT 1");
		try {
			while (rs.next()) {
				String preguntaTexto = rs.getString(2);
				String respuestaTexto = rs.getString(3);
				try {
					pregunta = new Pregunta(id, preguntaTexto, respuestaTexto);
				} catch (ExcepcionIngresoPalabras e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Optional.ofNullable(pregunta);

	}

	@Override
	public void save(Pregunta t) {
		//ResultSet rs = getDb().query("INSERT INTO public.pregunta (pregunta,respuesta,letra) VALUES ( "+ "'"+t.getPregunta()+"'::character varying, '"+t.getRespuesta()+ "'::character varying, '"+t.getLetra()+"'::character)"+ " returning id;");
		ResultSet rs = getDb().query("INSERT INTO public.pregunta (id,pregunta,respuesta,letra) VALUES ( "+ "'"+t.getId_pregunta()+"'::integer, '"+t.getPregunta()+"'::character varying, '"+t.getRespuesta()+ "'::character varying, '"+t.getLetra()+"'::character)"+ " returning id;");
			

	}

	@Override
	public void update(Pregunta t) {
		ResultSet rs = getDb().query("UPDATE public.pregunta SET pregunta = '"+t.getPregunta()+"'::character varying, "+"respuesta = '"+t.getRespuesta()+"'::character varying WHERE id = "+t.getId_pregunta()+";");
		
	}

	@Override
	public void delete(Pregunta t) {
		ResultSet rs = getDb().query("DELETE FROM public.pregunta WHERE id IN (" +t.getId_pregunta()+");");
		

	}
	
	
	public Integer maxId() {
		Integer id = 1;
		
		ResultSet rs = getDb().query("SELECT id FROM public.pregunta ORDER BY id DESC LIMIT 1;");
		try {
			if(rs.next()) {
				id = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return id;
	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

	@Override
	public Jugador get(String nombre) {
		// TODO Auto-generated method stub
		return null;
	}



}
