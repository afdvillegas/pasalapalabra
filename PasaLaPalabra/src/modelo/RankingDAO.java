package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RankingDAO implements Dao<Jugador> {

	private Database db = Database.getInstance();

	@Override
	public List<Jugador> getAll() {
		List<Jugador> jugadores = new ArrayList<Jugador>();
		ResultSet rs = getDb().query("SELECT * FROM jugador");
		try {
			while (rs.next()) {
				String nombre = rs.getString(1);
				Integer victorias = rs.getInt(2);
				Integer derrotas = rs.getInt(3);
				Integer puntuacionTotal = rs.getInt(4);
				Jugador jugador = new Jugador(nombre, 0, "", victorias, derrotas, puntuacionTotal);
				jugadores.add(jugador);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jugadores;
	}

	@Override
	public void save(Jugador t) {
		ResultSet rs = getDb().query("INSERT INTO public.jugador (nombre,victorias,derrotas,puntuacion) VALUES ( " + "'"
				+ t.getNombre() + "'::character varying, '" + t.getVictorias() + "'::integer, '" + t.getDerrotas()
				+ "'::integer, '" + t.getPuntuacionTotal() + "'::integer)" + " returning nombre;");

	}

	@Override
	public void update(Jugador t) {
		ResultSet rs = getDb().query("UPDATE public.jugador SET victorias = " + t.getVictorias() + "::integer, "+ "derrotas = " + t.getDerrotas() + "::integer, " + "puntuacion = " + t.getPuntuacionTotal()+ "::integer WHERE nombre ='" + t.getNombre() + "';");

	}

	@Override
	public void delete(Jugador t) {
		ResultSet rs = getDb().query("DELETE FROM public.jugador WHERE id IN (" + t.getNombre() + ");");

	}

	public Database getDb() {
		return db;
	}

	public void setDb(Database db) {
		this.db = db;
	}

	@Override
	public Optional<Jugador> get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Jugador get(String nombre) {
		Jugador jugador = null;
		ResultSet rs = getDb().query("SELECT * FROM jugador WHERE jugador.nombre= '" + nombre + "'");
		try {
			while (rs.next()) {
				Integer victorias = rs.getInt(2);
				Integer derrotas = rs.getInt(3);
				Integer puntuacion = rs.getInt(4);
				jugador = new Jugador(nombre, null, "", victorias, derrotas, puntuacion);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jugador;
	}

	public boolean verificarJugador(String nombre) {
		boolean res = false;
		ResultSet rs = getDb().query("SELECT * FROM jugador WHERE jugador.nombre='" + nombre+"';");
		try {

			if (rs.last()) {
				res = true;
			}
		} catch (Exception e) {
			System.err.print("Ha ocurrido un error: " + e.getMessage());
		}
		return res;
	}
}
