package modelo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Reportes {

	
	public static void reportesJugadores() throws SQLException {
		String consulta = "Select * FROM jugador";
		ResultSet rs = Database.getInstance().query(consulta);
		 
		
	AbstractJasperReports.createReport(rs, "Reportes\\ReporteJugadores.jasper");
				
	AbstractJasperReports.exportToPDF("Reportes\\reporteJugadores.pdf"); 
	}
	
	public static void reporteMejoresJugadores() throws SQLException{
		String consulta ="select * from jugador order by victorias desc limit 3";
	
		ResultSet rs = Database.getInstance().query(consulta);
		AbstractJasperReports.createReport(rs, "Reportes\\MejoresJugadores.jasper");
		
		AbstractJasperReports.exportToPDF("Reportes\\reporteTopJugadores.pdf");
		
	}
	
	public static void reportePreguntasPorLetra(Character letra) {
		
		String consulta = "Select * from pregunta where letra ="+"'"+letra+"'";
		
		HashMap<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("letraPregunta", letra.toString());
		
		ResultSet rs = Database.getInstance().query(consulta);
	//	AbstractJasperReports.createReport(rs, "Reportes\\ReportePreguntasLetra.jasper");
		AbstractJasperReports.createReportParametro(rs, "Reportes\\ReportePreguntasLetra.jasper", parametros);
		AbstractJasperReports.exportToPDF("Reportes\\reportePreguntasPorLetra.pdf");
	}
	
	
}
