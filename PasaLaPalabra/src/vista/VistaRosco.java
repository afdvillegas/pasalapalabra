package vista;

import java.awt.Container;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import modelo.Pregunta;

public class VistaRosco extends JLabel {
	
	private JLabel lblletra = new JLabel();
	private String letraas;


	public VistaRosco() {
		super();
		this.setLblletra(new JLabel());
		this.setLetraas(letraas);
		
	}

	public String getLetraas() {
		return letraas;
	}

	public void setLetraas(String letraas) {
		this.letraas = letraas;
	}
	 
	

	public JLabel getLblletra() {
		return lblletra;
	}

	public void setLblletra(JLabel lblletra) {
		this.lblletra = lblletra;
	}
	public void inicioRosco() {
	
	
	
	}

	public void setteoRosco(ArrayList<Pregunta>listapregunta) {
		for (Pregunta pregunta2 : listapregunta) {
			cambioColor(pregunta2);

		}
	}
	
	public ImageIcon cambioColor(Pregunta pre) {
		
		
		ImageIcon imageIcon=null;
		if (pre.getEstadoP().equals("correcto")) {
		
			 imageIcon = new ImageIcon(
					new ImageIcon("Letras\\VERDES\\" + pre.getLetra() + ".png").getImage().getScaledInstance(52, 70, Image.SCALE_DEFAULT));
			lblletra.setIcon(imageIcon);		
		} else if (pre.getEstadoP().equals("incorrecto")) {
			 imageIcon = new ImageIcon(
					new ImageIcon("Letras\\ROJOS\\" + pre.getLetra() + ".png").getImage().getScaledInstance(52, 70, Image.SCALE_DEFAULT));
			lblletra.setIcon(imageIcon);	
			
		} else if (pre.getEstadoP().equals("paso")) {
		 imageIcon = new ImageIcon(
					new ImageIcon("Letras\\AMARILLOS\\" + pre.getLetra() + ".png").getImage().getScaledInstance(52, 70, Image.SCALE_DEFAULT));
			lblletra.setIcon(imageIcon);	
			
		} else if (pre.getEstadoP().equals("sin responder")) {
		 imageIcon = new ImageIcon(
					new ImageIcon("Letras\\AZUL\\" + pre.getLetra() + ".png").getImage().getScaledInstance(52, 70, Image.SCALE_DEFAULT));
			lblletra.setIcon(imageIcon);	

		}
		System.out.println("holaaa");
		System.out.println(pre.getLetra());
	return imageIcon;
}


}
