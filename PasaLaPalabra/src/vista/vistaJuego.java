package vista;

import java.awt.Color;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import controlador.ControladorJuego;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.SwingConstants;

public class vistaJuego extends JFrame {

	public JLabel getLblletraQue() {
		return lblletraQue;
	}

	public void setLblletraQue(JLabel lblletraQue) {
		this.lblletraQue = lblletraQue;
	}

	public JLabel getLblletraQ2() {
		return lblletraQ2;
	}

	public void setLblletraQ2(JLabel lblletraQ2) {
		this.lblletraQ2 = lblletraQ2;
	}

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ControladorJuego cj;
	private JTextField txtRespuesta;
	private JLabel lblRtsC2;
	private JLabel lblRtsC2_1;
	private JLabel labelFondo;
	private JLabel lblPregunta;
	private JLabel lblNewLabelugador1;
	private JLabel lblNewLabelJugador2;
	private JLabel lblTiempo1;
	private JLabel lblTiempo2;
	private JLabel lblcorractas;
	private JLabel lblcorractas_1;
	private JLabel lblincorrectas;
	private JLabel lblincorrectas_1;
	private JLabel lblPaso;
	private JLabel lblPaso_1;
	private JLabel lblA;
	private JLabel lblB;
	private JLabel lblC;
	private JLabel lblD;
	private JLabel lblE;
	private JLabel lblF;
	private JLabel lblG;
	private JLabel lblH;
	private JLabel lblI;
	private JLabel lblJ;
	private JLabel lblK;
	private JLabel lblL;
	private JLabel lblM;
	private JLabel lblN;
	private JLabel lblO;
	private JLabel lblP;
	private JLabel lblQ;
	private JLabel lblR;
	private JLabel lblS;
	private JLabel lblT;
	private JLabel lblU;
	private JLabel lblV;
	private JLabel lblW;
	private JLabel lblX;
	private JLabel lblY;
	private JLabel lblZ;
	private JLabel lblLetraPantalla;
	private JLabel  lblletraQue;
	private JLabel lblletraQ2;

	public vistaJuego(ControladorJuego cj) {
		this.setCj(cj);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1040, 720);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		txtRespuesta = new JTextField();
		txtRespuesta.setForeground(Color.WHITE);
		txtRespuesta.setBackground(new Color(30, 144, 255));
		txtRespuesta.setFont(new Font("Showcard Gothic", Font.PLAIN, 14));
		txtRespuesta.setBounds(447, 378, 131, 38);
		txtRespuesta.setText("");
		txtRespuesta.addKeyListener(this.getCj());
		contentPane.add(txtRespuesta);
		txtRespuesta.setColumns(10);

		JLabel lblPasapalabra = new JLabel("Pasalapalabra");
		lblPasapalabra.setFont(new Font("Showcard Gothic", Font.BOLD | Font.ITALIC, 20));
		lblPasapalabra.setForeground(new Color(255, 140, 0));
		lblPasapalabra.setBounds(407, 11, 259, 32);
		contentPane.add(lblPasapalabra);

		lblA = new JLabel("A");
		lblA.setBounds(489, 46, 52, 70);
		contentPane.add(lblA);

		lblB = new JLabel("B");
		lblB.setBounds(551, 54, 52, 70);
		contentPane.add(lblB);

		lblC = new JLabel("C");
		lblC.setBounds(614, 77, 52, 70);
		contentPane.add(lblC);

		lblD = new JLabel("D");
		lblD.setBounds(667, 120, 52, 70);
		contentPane.add(lblD);

		lblE = new JLabel("E");
		lblE.setBounds(704, 162, 52, 70);
		contentPane.add(lblE);

		lblF = new JLabel("F");
		lblF.setBounds(724, 220, 52, 70);
		contentPane.add(lblF);

		lblG = new JLabel("G");
		lblG.setBounds(734, 280, 52, 70);
		contentPane.add(lblG);

		lblH = new JLabel("H");
		lblH.setBounds(724, 346, 52, 70);
		contentPane.add(lblH);

		lblI = new JLabel("I");
		lblI.setBounds(704, 390, 52, 70);
		contentPane.add(lblI);

		lblJ = new JLabel("J");
		lblJ.setBounds(679, 437, 52, 70);
		contentPane.add(lblJ);

		lblK = new JLabel("K");
		lblK.setBounds(636, 480, 52, 70);
		contentPane.add(lblK);

		lblL = new JLabel("L");
		lblL.setBounds(589, 504, 52, 70);
		contentPane.add(lblL);

		lblM = new JLabel("M");
		lblM.setBounds(541, 512, 52, 70);
		contentPane.add(lblM);

		lblN = new JLabel("N");
		lblN.setBounds(479, 512, 52, 70);
		contentPane.add(lblN);

		lblO = new JLabel("O");
		lblO.setBounds(417, 512, 52, 70);
		contentPane.add(lblO);

		lblP = new JLabel("P");
		lblP.setBounds(355, 480, 52, 70);
		contentPane.add(lblP);

		lblQ = new JLabel("Q");
		lblQ.setBounds(311, 441, 52, 70);
		contentPane.add(lblQ);

		lblR = new JLabel("R");
		lblR.setBounds(274, 390, 52, 70);
		contentPane.add(lblR);

		lblS = new JLabel("S");
		lblS.setBounds(241, 340, 52, 70);
		contentPane.add(lblS);

		lblT = new JLabel("T");
		lblT.setBounds(241, 285, 52, 70);
		contentPane.add(lblT);

		lblU = new JLabel("U");
		lblU.setBounds(252, 230, 52, 70);
		contentPane.add(lblU);

		lblV = new JLabel("V");
		lblV.setBounds(270, 176, 52, 70);
		contentPane.add(lblV);
		
		lblW = new JLabel("W");
		lblW.setBounds(299, 130, 52, 70);
		contentPane.add(lblW);

		lblX = new JLabel("X");
		lblX.setBounds(336, 98, 52, 70);
		contentPane.add(lblX);

		lblY = new JLabel("Y");
		lblY.setBounds(379, 77, 52, 70);
		contentPane.add(lblY);

		lblZ = new JLabel("Z");
		lblZ.setBounds(430, 54, 52, 70);
		contentPane.add(lblZ);

		lblPregunta = new JLabel("             Pregunta");

		lblPregunta.setFont(new Font("Showcard Gothic", Font.BOLD, 13));

		lblPregunta.setBounds(390, 170, 231, 172);
		lblPregunta.setForeground(new Color(220, 20, 60));
		contentPane.add(lblPregunta);

		lblNewLabelugador1 = new JLabel("New label");
		lblNewLabelugador1.setForeground(new Color(255, 255, 0));
		lblNewLabelugador1.setFont(new Font("Showcard Gothic", Font.PLAIN, 24));
		lblNewLabelugador1.setBounds(44, 77, 185, 45);
		contentPane.add(lblNewLabelugador1);

		lblNewLabelJugador2 = new JLabel("New label");
		lblNewLabelJugador2.setForeground(new Color(255, 255, 0));
		lblNewLabelJugador2.setFont(new Font("Showcard Gothic", Font.PLAIN, 24));
		lblNewLabelJugador2.setBounds(800, 77, 193, 55);
		contentPane.add(lblNewLabelJugador2);

		lblTiempo1 = new JLabel(" ");
		lblTiempo1.setForeground(Color.WHITE);
		lblTiempo1.setFont(new Font("Showcard Gothic", Font.BOLD, 60));
		lblTiempo1.setBounds(53, 523, 131, 86);
		contentPane.add(lblTiempo1);

		lblTiempo2 = new JLabel(" ");
		lblTiempo2.setForeground(Color.WHITE);
		lblTiempo2.setFont(new Font("Showcard Gothic", Font.BOLD, 60));
		lblTiempo2.setBounds(885, 533, 108, 97);
		contentPane.add(lblTiempo2);

		lblRtsC2 = new JLabel("0");
		lblRtsC2.setForeground(Color.WHITE);
		lblRtsC2.setFont(new Font("Showcard Gothic", Font.PLAIN, 45));
		lblRtsC2.setBounds(784, 590, 60, 70);
		contentPane.add(lblRtsC2);

		lblRtsC2_1 = new JLabel(" ");
		lblRtsC2_1.setForeground(Color.WHITE);
		lblRtsC2_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 45));
		lblRtsC2_1.setBounds(213, 578, 46, 70);
		contentPane.add(lblRtsC2_1);

		lblcorractas = new JLabel("Bien:");
		lblcorractas.setForeground(Color.GREEN);
		lblcorractas.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblcorractas.setBounds(10, 145, 219, 55);
		contentPane.add(lblcorractas);

		lblincorrectas = new JLabel("Mal:");
		lblincorrectas.setForeground(Color.RED);
		lblincorrectas.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblincorrectas.setBounds(10, 204, 219, 55);
		contentPane.add(lblincorrectas);

		lblPaso = new JLabel("Paso:");
		lblPaso.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblPaso.setForeground(Color.ORANGE);
		lblPaso.setBounds(10, 264, 219, 55);
		contentPane.add(lblPaso);

		lblcorractas_1 = new JLabel("bien:");
		lblcorractas_1.setForeground(Color.GREEN);
		lblcorractas_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblcorractas_1.setBounds(800, 145, 219, 55);
		contentPane.add(lblcorractas_1);

		lblincorrectas_1 = new JLabel("mal:");
		lblincorrectas_1.setForeground(Color.RED);
		lblincorrectas_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblincorrectas_1.setBounds(800, 204, 219, 55);
		contentPane.add(lblincorrectas_1);

		lblPaso_1 = new JLabel("paso:");
		lblPaso_1.setForeground(Color.ORANGE);
		lblPaso_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblPaso_1.setBounds(800, 264, 219, 55);
		contentPane.add(lblPaso_1);
		
		lblLetraPantalla = new JLabel("A");
		lblLetraPantalla.setForeground(new Color(139, 0, 0));
		lblLetraPantalla.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetraPantalla.setFont(new Font("Showcard Gothic", Font.BOLD, 26));
		lblLetraPantalla.setBounds(470, 162, 84, 45);
		contentPane.add(lblLetraPantalla);

		
		 lblletraQue = new JLabel("");
		lblletraQue.setFont(new Font("Showcard Gothic", Font.PLAIN, 18));
		lblletraQue.setForeground(new Color(244, 164, 96));
		lblletraQue.setBounds(20, 330, 138, 38);
		contentPane.add(lblletraQue);
		
		 lblletraQ2 = new JLabel("");
		lblletraQ2.setForeground(new Color(244, 164, 96));
		lblletraQ2.setFont(new Font("Showcard Gothic", Font.PLAIN, 18));
		lblletraQ2.setBounds(796, 330, 164, 38);
		contentPane.add(lblletraQ2);
		
		
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\fondo12.png").getImage().getScaledInstance(1024, 720, Image.SCALE_DEFAULT));
		labelFondo = new JLabel("");
		labelFondo.setForeground(Color.WHITE);
		labelFondo.setFont(new Font("Arial", Font.BOLD, 14));
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 1024, 720);
		contentPane.add(labelFondo);
		
	
		


		// Actualiza el icono

		repaint();

	}

	public ControladorJuego getCj() {
		return cj;
	}

	public void setCj(ControladorJuego cj) {
		this.cj = cj;
	}

	public JTextField getTxtRespuesta() {
		return txtRespuesta;
	}

	public void setTxtRespuesta(JTextField txtRespuesta) {
		this.txtRespuesta = txtRespuesta;
	}

	public JLabel getLabelRespuesta() {
		return labelFondo;
	}

	public void setLabelRespuesta(JLabel labelRespuesta) {
		this.labelFondo = labelRespuesta;
	}

	public JLabel getLblPregunta() {
		return lblPregunta;
	}

	public void setLblPregunta(JLabel lblPregunta) {
		this.lblPregunta = lblPregunta;
	}

	public JLabel getLblNewLabelugador1() {
		return lblNewLabelugador1;
	}

	public void setLblNewLabelugador1(JLabel lblNewLabelugador1) {
		this.lblNewLabelugador1 = lblNewLabelugador1;
	}

	public JLabel getLblNewLabelJugador2() {
		return lblNewLabelJugador2;
	}

	public void setLblNewLabelJugador2(JLabel lblNewLabelJugador2) {
		this.lblNewLabelJugador2 = lblNewLabelJugador2;
	}

	public JLabel getLblA() {
		return lblA;
	}

	public void setLblA(JLabel lblA) {
		this.lblA = lblA;
	}

	public JLabel getLblB() {
		return lblB;
	}

	public void setLblB(JLabel lblB) {
		this.lblB = lblB;
	}

	public JLabel getLblC() {
		return lblC;
	}

	public void setLblC(JLabel lblC) {
		this.lblC = lblC;
	}

	public JLabel getLblD() {
		return lblD;
	}

	public void setLblD(JLabel lblD) {
		this.lblD = lblD;
	}

	public JLabel getLblE() {
		return lblE;
	}

	public void setLblE(JLabel lblE) {
		this.lblE = lblE;
	}

	public JLabel getLblF() {
		return lblF;
	}

	public void setLblF(JLabel lblF) {
		this.lblF = lblF;
	}

	public JLabel getLblG() {
		return lblG;
	}

	public void setLblG(JLabel lblG) {
		this.lblG = lblG;
	}

	public JLabel getLblH() {
		return lblH;
	}

	public void setLblH(JLabel lblH) {
		this.lblH = lblH;
	}

	public JLabel getLblJ() {
		return lblJ;
	}

	public void setLblJ(JLabel lblJ) {
		this.lblJ = lblJ;
	}

	public JLabel getLblK() {
		return lblK;
	}

	public void setLblK(JLabel lblK) {
		this.lblK = lblK;
	}

	public JLabel getLblL() {
		return lblL;
	}

	public void setLblL(JLabel lblL) {
		this.lblL = lblL;
	}

	public JLabel getLblM() {
		return lblM;
	}

	public void setLblM(JLabel lblM) {
		this.lblM = lblM;
	}

	public JLabel getLblN() {
		return lblN;
	}

	public void setLblN(JLabel lblN) {
		this.lblN = lblN;
	}

	public JLabel getLblO() {
		return lblO;
	}

	public void setLblO(JLabel lblO) {
		this.lblO = lblO;
	}

	public JLabel getLblP() {
		return lblP;
	}

	public void setLblP(JLabel lblP) {
		this.lblP = lblP;
	}

	public JLabel getLblQ() {
		return lblQ;
	}

	public void setLblQ(JLabel lblQ) {
		this.lblQ = lblQ;
	}

	public JLabel getLblR() {
		return lblR;
	}

	public void setLblR(JLabel lblR) {
		this.lblR = lblR;
	}

	public JLabel getLblS() {
		return lblS;
	}

	public void setLblS(JLabel lblS) {
		this.lblS = lblS;
	}

	public JLabel getLblT() {
		return lblT;
	}

	public void setLblT(JLabel lblT) {
		this.lblT = lblT;
	}

	public JLabel getLblU() {
		return lblU;
	}

	public void setLblU(JLabel lblU) {
		this.lblU = lblU;
	}

	public JLabel getLblV() {
		return lblV;
	}

	public void setLblV(JLabel lblV) {
		this.lblV = lblV;
	}
	

	public JLabel getLblW() {
		return lblW;
	}

	public void setLblW(JLabel lblW) {
		this.lblW = lblW;
	}

	public JLabel getLblX() {
		return lblX;
	}

	public void setLblX(JLabel lblX) {
		this.lblX = lblX;
	}

	public JLabel getLblY() {
		return lblY;
	}

	public void setLblY(JLabel lblY) {
		this.lblY = lblY;
	}

	public JLabel getLblZ() {
		return lblZ;
	}

	public void setLblZ(JLabel lblZ) {
		this.lblZ = lblZ;
	}

	public JLabel getLblI() {
		return lblI;
	}

	public void setLblI(JLabel lblI) {
		this.lblI = lblI;
	}

	public JLabel getLblTiempo1() {
		return lblTiempo1;
	}

	public void setLblTiempo1(JLabel lblTiempo1) {
		this.lblTiempo1 = lblTiempo1;
	}

	public JLabel getLblRtsC2() {
		return lblRtsC2;
	}

	public void setLblRtsC2(JLabel lblRtsC2) {
		this.lblRtsC2 = lblRtsC2;
	}

	public JLabel getLblRtsC2_1() {
		return lblRtsC2_1;
	}

	public void setLblRtsC2_1(JLabel lblRtsC2_1) {
		this.lblRtsC2_1 = lblRtsC2_1;
	}

	public JLabel getLabelFondo() {
		return labelFondo;
	}

	public void setLabelFondo(JLabel labelFondo) {
		this.labelFondo = labelFondo;
	}

	public JLabel getLblTiempo2() {
		return lblTiempo2;
	}

	public void setLblTiempo2(JLabel lblTiempo2) {
		this.lblTiempo2 = lblTiempo2;
	}

	public JLabel getLblcorractas() {
		return lblcorractas;
	}

	public void setLblcorractas(JLabel lblcorractas) {
		this.lblcorractas = lblcorractas;
	}

	public JLabel getLblincorrectas() {
		return lblincorrectas;
	}

	public void setLblincorrectas(JLabel lblincorrectas) {
		this.lblincorrectas = lblincorrectas;
	}

	public JLabel getLblPaso() {
		return lblPaso;
	}

	public void setLblPaso(JLabel lblPaso) {
		this.lblPaso = lblPaso;
	}

	public JLabel getLblcorractas_1() {
		return lblcorractas_1;
	}

	public void setLblcorractas_1(JLabel lblcorractas_1) {
		this.lblcorractas_1 = lblcorractas_1;
	}

	public JLabel getLblincorrectas_1() {
		return lblincorrectas_1;
	}

	public void setLblincorrectas_1(JLabel lblincorrectas_1) {
		this.lblincorrectas_1 = lblincorrectas_1;
	}

	public JLabel getLblPaso_1() {
		return lblPaso_1;
	}

	public void setLblPaso_1(JLabel lblPaso_1) {
		this.lblPaso_1 = lblPaso_1;
	}

	public JLabel getLblLetraPantalla() {
		return lblLetraPantalla;
	}

	public void setLblLetraPantalla(JLabel lblLetraPantalla) {
		this.lblLetraPantalla = lblLetraPantalla;
	}
}
