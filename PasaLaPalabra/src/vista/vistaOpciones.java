package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorOpciones;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class vistaOpciones extends JFrame {


	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldJugador1;
	private JTextField textFieldJugador2;
	private JComboBox<String> comboBoxDificultad;
	private JRadioButton rdbtnJugador;
	private JRadioButton rdbtnCpu;
	private JLabel lblOpcionesGenerales;
	private JComboBox<String> comboBoxMusica;
	private JComboBox<String> comboBoxTiempo;
	private JButton btnJugar;
	private ControladorOpciones co;
	private JButton btnVolver;
	private JButton btnPreguntas;
	private JLabel lblJugador_1;
	private JLabel lblcpu;
	private JLabel lblIngreseNombre;
	private JLabel lblIngreseNombre_1;

	public vistaOpciones(ControladorOpciones co) {
		this.setCo(co);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 700);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\opcionjuego.png").getImage().getScaledInstance(600, 700, Image.SCALE_DEFAULT));

		JLabel lblJugador = new JLabel("Jugador 1");
		lblJugador.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		lblJugador.setForeground(new Color(255, 140, 0));
		lblJugador.setBackground(new Color(139, 0, 139));
		lblJugador.setBounds(95, 133, 120, 27);
		contentPane.add(lblJugador);

		textFieldJugador1 = new JTextField();
		textFieldJugador1.setFont(new Font("Showcard Gothic", Font.BOLD, 13));
		textFieldJugador1.setForeground(new Color(255, 255, 0));
		textFieldJugador1.setBackground(new Color(139, 0, 139));
		textFieldJugador1.setBounds(311, 133, 103, 23);
		contentPane.add(textFieldJugador1);
		textFieldJugador1.setColumns(10);

		JLabel lblContrincante = new JLabel("Contrincante");
		lblContrincante.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		lblContrincante.setForeground(new Color(255, 165, 0));
		lblContrincante.setBounds(155, 183, 188, 30);
		contentPane.add(lblContrincante);
		
		
		
		ButtonGroup enemigos = new ButtonGroup();
		
		lblJugador_1 = new JLabel(" Jugador 2");
		lblJugador_1.setForeground(new Color(255, 140, 0));
		lblJugador_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		lblJugador_1.setBackground(new Color(139, 0, 139));
		lblJugador_1.setBounds(115, 239, 120, 27);
		contentPane.add(lblJugador_1);
		
		
		lblcpu = new JLabel("CPU");
		lblcpu.setForeground(new Color(255, 140, 0));
		lblcpu.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		lblcpu.setBackground(new Color(139, 0, 139));
		lblcpu.setBounds(115, 327, 120, 27);
		contentPane.add(lblcpu);
	
		
		rdbtnJugador = new JRadioButton("");
		rdbtnJugador.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		rdbtnJugador.setForeground(new Color(255, 255, 0));
		rdbtnJugador.setBackground(new Color(139, 0, 139));
		rdbtnJugador.setBounds(88, 239, 21, 23);
		rdbtnJugador.setSelected(true);
		contentPane.add(rdbtnJugador);

		rdbtnCpu = new JRadioButton("");
		rdbtnCpu.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		rdbtnCpu.setForeground(new Color(255, 255, 0));
		rdbtnCpu.setBackground(new Color(139, 0, 139));
		rdbtnCpu.setBounds(88, 331, 21, 23);
		contentPane.add(rdbtnCpu);
		
		enemigos.add(rdbtnJugador);
		enemigos.add(rdbtnCpu);
		
		

		textFieldJugador2 = new JTextField();
		textFieldJugador2.setFont(new Font("Showcard Gothic", Font.BOLD, 13));
		textFieldJugador2.setBackground(new Color(139, 0, 139));
		textFieldJugador2.setForeground(new Color(255, 255, 0));
		textFieldJugador2.setBounds(311, 240, 103, 30);
		contentPane.add(textFieldJugador2);
		textFieldJugador2.setColumns(10);

		comboBoxDificultad = new JComboBox<String>();
		comboBoxDificultad.setFont(new Font("Showcard Gothic", Font.PLAIN, 12));
		comboBoxDificultad.setForeground(new Color(255, 255, 0));
		comboBoxDificultad.setBackground(new Color(139, 0, 139));
		comboBoxDificultad.setBounds(311, 323, 103, 31);
		comboBoxDificultad.addItem("Facil");
		comboBoxDificultad.addItem("Normal");
		comboBoxDificultad.addItem("Dificil");
		contentPane.add(comboBoxDificultad);

		lblOpcionesGenerales = new JLabel("Opciones generales");
		lblOpcionesGenerales.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		lblOpcionesGenerales.setForeground(new Color(255, 140, 0));
		lblOpcionesGenerales.setBounds(159, 389, 215, 27);
		contentPane.add(lblOpcionesGenerales);

		comboBoxMusica = new JComboBox<String>();
		comboBoxMusica.setFont(new Font("Showcard Gothic", Font.PLAIN, 15));
		comboBoxMusica.setForeground(new Color(255, 255, 0));
		comboBoxMusica.setBackground(new Color(139, 0, 139));
		comboBoxMusica.setBounds(51, 465, 129, 27);
		comboBoxMusica.addItem("Cancion ");
		comboBoxMusica.addItem("Cancion 1");
		comboBoxMusica.addItem("Cancion 2");
		comboBoxMusica.addItem("Cancion 3");


		contentPane.add(comboBoxMusica);

		comboBoxTiempo = new JComboBox<String>();
		comboBoxTiempo.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		comboBoxTiempo.setForeground(new Color(255, 255, 0));
		comboBoxTiempo.setBackground(new Color(139, 0, 139));
		comboBoxTiempo.setBounds(391, 463, 120, 30);
		comboBoxTiempo.addItem("120");
		comboBoxTiempo.addItem("90");
		comboBoxTiempo.addItem("60");
		
		contentPane.add(comboBoxTiempo);

		btnJugar = new JButton("Jugar");
		btnJugar.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		btnJugar.setForeground(new Color(255, 255, 0));
		btnJugar.setBackground(new Color(139, 0, 139));
		
		btnJugar.addActionListener(this.getCo());
		
		btnJugar.setBounds(216, 575, 140, 75);
		contentPane.add(btnJugar);

		btnVolver = new JButton("Volver");
		btnVolver.setFont(new Font("Showcard Gothic", Font.PLAIN, 11));
		btnVolver.setForeground(new Color(255, 255, 0));
		btnVolver.setBackground(new Color(139, 0, 139));
		btnVolver.addActionListener(this.getCo());
		
		btnVolver.setBounds(10, 623, 86, 27);
		contentPane.add(btnVolver);
		
		btnPreguntas = new JButton("Preguntas");
		btnPreguntas.setFont(new Font("Showcard Gothic", Font.PLAIN, 15));
		btnPreguntas.setForeground(new Color(255, 255, 0));
		btnPreguntas.setBackground(new Color(139, 0, 139));
		
		btnPreguntas.addActionListener(this.getCo());
		btnPreguntas.setBounds(204, 463, 158, 30);
		contentPane.add(btnPreguntas);


		JLabel lblNewLabel = new JLabel("     musica");
		lblNewLabel.setForeground(new Color(255, 140, 0));
		lblNewLabel.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblNewLabel.setBounds(51, 438, 129, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("     tiempo");
		lblNewLabel_1.setForeground(new Color(255, 140, 0));
		lblNewLabel_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(391, 438, 120, 14);
		contentPane.add(lblNewLabel_1);
		
		lblIngreseNombre = new JLabel("Ingrese nombre");
		lblIngreseNombre.setForeground(Color.RED);
		lblIngreseNombre.setBackground(Color.WHITE);
		lblIngreseNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIngreseNombre.setVisible(false);
		lblIngreseNombre.setBounds(424, 141, 112, 14);
		contentPane.add(lblIngreseNombre);
		
		lblIngreseNombre_1 = new JLabel("Ingrese nombre");
		lblIngreseNombre_1.setForeground(Color.RED);
		lblIngreseNombre_1.setBackground(Color.WHITE);
		lblIngreseNombre_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIngreseNombre_1.setVisible(false);
		lblIngreseNombre_1.setBounds(424, 247, 112, 14);
		contentPane.add(lblIngreseNombre_1);
		
		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0,0, 600, 700);
		contentPane.add(labelFondo);
		

		
		
		
	
		
		
	}

	public JButton getBtnPreguntas() {
		return btnPreguntas;
	}

	public void setBtnPreguntas(JButton btnPreguntas) {
		this.btnPreguntas = btnPreguntas;
	}

	public ControladorOpciones getCo() {
		return co;
	}

	public void setCo(ControladorOpciones co) {
		this.co = co;
	}

	public JTextField getTextFieldJugador1() {
		return textFieldJugador1;
	}

	public void setTextFieldJugador1(JTextField textFieldJugador1) {
		this.textFieldJugador1 = textFieldJugador1;
	}

	public JTextField getTextFieldJugador2() {
		return textFieldJugador2;
	}

	public void setTextFieldJugador2(JTextField textFieldJugador2) {
		this.textFieldJugador2 = textFieldJugador2;
	}

	public JComboBox<String> getComboBoxDificultad() {
		return comboBoxDificultad;
	}

	public void setComboBoxDificultad(JComboBox<String> comboBoxDificultad) {
		this.comboBoxDificultad = comboBoxDificultad;
	}

	public JRadioButton getRdbtnJugador() {
		return rdbtnJugador;
	}

	public void setRdbtnJugador(JRadioButton rdbtnJugador) {
		this.rdbtnJugador = rdbtnJugador;
	}

	public JRadioButton getRdbtnCpu() {
		return rdbtnCpu;
	}

	public void setRdbtnCpu(JRadioButton rdbtnCpu) {
		this.rdbtnCpu = rdbtnCpu;
	}

	public JLabel getLblOpcionesGenerales() {
		return lblOpcionesGenerales;
	}

	public void setLblOpcionesGenerales(JLabel lblOpcionesGenerales) {
		this.lblOpcionesGenerales = lblOpcionesGenerales;
	}

	public JComboBox getComboBoxMusica() {
		return comboBoxMusica;
	}

	public void setComboBoxMusica(JComboBox comboBoxMusica) {
		this.comboBoxMusica = comboBoxMusica;
	}

	public JComboBox<String> getComboBoxTiempo() {
		return comboBoxTiempo;
	}

	public void setComboBoxTiempo(JComboBox<String> comboBoxTiempo) {
		this.comboBoxTiempo = comboBoxTiempo;
	}

	public JButton getBtnJugar() {
		return btnJugar;
	}

	public void setBtnJugar(JButton btnJugar) {
		this.btnJugar = btnJugar;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

	public JLabel getLblIngreseNombre() {
		return lblIngreseNombre;
	}

	public void setLblIngreseNombre(JLabel lblIngreseNombre) {
		this.lblIngreseNombre = lblIngreseNombre;
	}

	public JLabel getLblIngreseNombre_1() {
		return lblIngreseNombre_1;
	}

	public void setLblIngreseNombre_1(JLabel lblIngreseNombre_1) {
		this.lblIngreseNombre_1 = lblIngreseNombre_1;
	}
	
}
