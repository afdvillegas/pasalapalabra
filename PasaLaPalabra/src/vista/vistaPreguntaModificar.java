package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPreguntasModificar;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;

public class vistaPreguntaModificar extends JFrame {

	private JPanel contentPane;
	private ControladorPreguntasModificar cpm;
	private JTextField textFieldPregunta;
	private JTextField textFieldRespuesta;
	private JButton btnModificar;
	private JButton btnVolver;

	public vistaPreguntaModificar(ControladorPreguntasModificar cpm) {
		this.setCpm(cpm);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\modificop.jpg").getImage().getScaledInstance(500, 550, Image.SCALE_DEFAULT));

		JLabel lblPregunta = new JLabel("Pregunta: ");
		lblPregunta.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		lblPregunta.setForeground(new Color(255, 140, 0));
		lblPregunta.setBounds(10, 154, 103, 14);
		contentPane.add(lblPregunta);

		textFieldPregunta = new JTextField();
		textFieldPregunta.setBounds(10, 188, 464, 20);
		contentPane.add(textFieldPregunta);
		textFieldPregunta.setColumns(10);

		JLabel lblRespuesta = new JLabel("Respuesta: ");
		lblRespuesta.setForeground(new Color(255, 140, 0));
		lblRespuesta.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		lblRespuesta.setBounds(10, 238, 115, 14);
		contentPane.add(lblRespuesta);

		textFieldRespuesta = new JTextField();
		textFieldRespuesta.setBounds(20, 263, 357, 20);
		contentPane.add(textFieldRespuesta);
		textFieldRespuesta.setColumns(10);

		btnModificar = new JButton("Modificar");
		btnModificar.setBackground(new Color(139, 0, 139));
		btnModificar.setForeground(new Color(255, 255, 0));
		btnModificar.setFont(new Font("Showcard Gothic", Font.PLAIN, 14));
		btnModificar.setBounds(169, 329, 133, 47);
		btnModificar.addActionListener(this.getCpm());
		contentPane.add(btnModificar);

		btnVolver = new JButton("Volver");
		btnVolver.setBackground(new Color(139, 0, 139));
		btnVolver.setForeground(new Color(255, 255, 0));
		btnVolver.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		btnVolver.setBounds(187, 415, 103, 29);
		btnVolver.addActionListener(this.getCpm());
		contentPane.add(btnVolver);

		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 500, 500);
		contentPane.add(labelFondo);
	}

	public ControladorPreguntasModificar getCpm() {
		return cpm;
	}

	public void setCpm(ControladorPreguntasModificar cpm) {
		this.cpm = cpm;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JTextField getTextFieldPregunta() {
		return textFieldPregunta;
	}

	public void setTextFieldPregunta(JTextField textFieldPregunta) {
		this.textFieldPregunta = textFieldPregunta;
	}

	public JTextField getTextFieldRespuesta() {
		return textFieldRespuesta;
	}

	public void setTextFieldRespuesta(JTextField textFieldRespuesta) {
		this.textFieldRespuesta = textFieldRespuesta;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public void setBtnVolver(JButton btnVolver) {
		this.btnVolver = btnVolver;
	}

}
