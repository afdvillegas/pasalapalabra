package vista;

import java.awt.Font;
import java.awt.Image;
import java.awt.ScrollPane;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorPreguntas;
import javax.swing.JScrollPane;
import java.awt.Color;

public class vistaPreguntas extends JFrame {

	private JPanel contentPane;
	private ControladorPreguntas cp;
	private JButton btnvolver;
	private JTable table;
	private JScrollPane scrollPane;
	private JButton btnAgregar;
	private JButton btnEliminar;
	private DefaultTableModel modelotabla;
	private JButton btnModificar;
	private JButton btnReporte;
	

	public vistaPreguntas(ControladorPreguntas cp) {
		this.setCp(cp);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(61, 105, 352, 309);
		contentPane.add(scrollPane);
		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\pregunta.jpg").getImage().getScaledInstance(500, 600, Image.SCALE_DEFAULT));
		
		modelotabla = new DefaultTableModel(new Object[][] {}, new String[] { "    Pregunta", "    Respuesta" });
		
		table = new JTable();
		table.setModel(modelotabla);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table);
		

		btnvolver = new JButton("Volver");
		btnvolver.setBackground(new Color(139, 0, 139));
		btnvolver.setForeground(new Color(255, 255, 0));
		btnvolver.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnvolver.setBounds(165, 508, 99, 30);
		btnvolver.addActionListener(this.getCp());
		contentPane.add(btnvolver);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(139, 0, 139));
		btnAgregar.setForeground(new Color(255, 255, 0));
		btnAgregar.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnAgregar.setBounds(27, 441, 99, 30);
		btnAgregar.addActionListener(this.getCp());
		contentPane.add(btnAgregar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBackground(new Color(139, 0, 139));
		btnEliminar.setForeground(new Color(255, 255, 0));
		btnEliminar.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnEliminar.setBounds(303, 441, 110, 30);
		btnEliminar.addActionListener(this.getCp());
		contentPane.add(btnEliminar);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBackground(new Color(139, 0, 139));
		btnModificar.setForeground(new Color(255, 255, 0));
		btnModificar.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnModificar.setBounds(136, 441, 157, 30);
		btnModificar.addActionListener(this.getCp());
		contentPane.add(btnModificar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setForeground(Color.YELLOW);
		btnReporte.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnReporte.setBackground(new Color(139, 0, 139));
		btnReporte.setBounds(303, 508, 110, 30);
		btnReporte.addActionListener(this.getCp());
		contentPane.add(btnReporte);
		
		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 500, 600);
		contentPane.add(labelFondo);
		

		this.setLocationRelativeTo(null);
	}

	public JButton getBtnvolver() {
		return btnvolver;
	}

	public void setBtnvolver(JButton btnvolver) {
		this.btnvolver = btnvolver;
	}

	public ControladorPreguntas getCp() {
		return cp;
	}

	public void setCp(ControladorPreguntas cp) {
		this.cp = cp;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public DefaultTableModel getModelotabla() {
		return modelotabla;
	}

	public void setModelotabla(DefaultTableModel modelotabla) {
		this.modelotabla = modelotabla;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public void setBtnReporte(JButton btnReporte) {
		this.btnReporte = btnReporte;
	}
	
	
	
}
