package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorPreguntasNueva;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

public class vistaPreguntasNueva extends JFrame {

	private JPanel contentPane;
	private ControladorPreguntasNueva cpn;
	private JTextField textFieldPregunta;
	private JTextField textFieldRespuesta;
	private JButton btnAgregar;
	private JButton btnCancelar;
	private JRadioButton rdbtnContiene;
	private JRadioButton rdbtnEmpiezaCon;
	private JComboBox<Character> comboBoxLetra;

	public vistaPreguntasNueva(ControladorPreguntasNueva cpn) {
		this.setCpn(cpn);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\pregunta.jpg").getImage().getScaledInstance(500, 500, Image.SCALE_DEFAULT));

		JLabel lblPregunta = new JLabel("Pregunta: ");
		lblPregunta.setForeground(new Color(255, 140, 0));
		lblPregunta.setFont(new Font("Showcard Gothic", Font.BOLD, 18));
		lblPregunta.setBounds(10, 144, 117, 14);
		contentPane.add(lblPregunta);

		JLabel lblRespuesta = new JLabel("Respuesta: ");
		lblRespuesta.setForeground(new Color(255, 140, 0));
		lblRespuesta.setFont(new Font("Showcard Gothic", Font.BOLD, 18));
		lblRespuesta.setBounds(10, 210, 125, 14);
		contentPane.add(lblRespuesta);

		textFieldPregunta = new JTextField();
		textFieldPregunta.setBounds(145, 141, 331, 20);
		contentPane.add(textFieldPregunta);
		textFieldPregunta.setColumns(10);

		textFieldRespuesta = new JTextField();
		textFieldRespuesta.setBounds(145, 207, 154, 20);
		contentPane.add(textFieldRespuesta);
		textFieldRespuesta.setColumns(10);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(139, 0, 139));
		btnAgregar.setForeground(new Color(255, 255, 0));
		btnAgregar.setFont(new Font("Showcard Gothic", Font.PLAIN, 12));
		btnAgregar.setBounds(279, 275, 117, 33);
		btnAgregar.addActionListener(this.getCpn());
		contentPane.add(btnAgregar);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBackground(new Color(139, 0, 139));
		btnCancelar.setForeground(new Color(255, 255, 0));
		btnCancelar.setFont(new Font("Showcard Gothic", Font.PLAIN, 12));
		btnCancelar.setBounds(106, 275, 125, 33);
		btnCancelar.addActionListener(this.getCpn());
		contentPane.add(btnCancelar);

		ButtonGroup tipoRespuesta = new ButtonGroup();

		comboBoxLetra = new JComboBox<Character>();
		comboBoxLetra.setFont(new Font("Showcard Gothic", Font.PLAIN, 12));
		comboBoxLetra.setBackground(new Color(139, 0, 139));
		comboBoxLetra.setForeground(new Color(255, 215, 0));
		comboBoxLetra.setBounds(409, 226, 67, 20);
		comboBoxLetra.addItem('K');
		comboBoxLetra.addItem('W');
		comboBoxLetra.addItem('X');
		comboBoxLetra.addItem('Y');
		contentPane.add(comboBoxLetra);
		
		rdbtnContiene = new JRadioButton("");
		rdbtnContiene.setBackground(new Color(0, 0, 139));
		rdbtnContiene.setBounds(305, 221, 21, 21);
		contentPane.add(rdbtnContiene);

		rdbtnEmpiezaCon = new JRadioButton("");
		rdbtnEmpiezaCon.setBackground(new Color(0, 0, 139));
		rdbtnEmpiezaCon.setBounds(305, 201, 21, 23);
		rdbtnEmpiezaCon.setSelected(true);
		contentPane.add(rdbtnEmpiezaCon);

		tipoRespuesta.add(rdbtnContiene);
		tipoRespuesta.add(rdbtnEmpiezaCon);

		JLabel lblNewLabel = new JLabel("Contiene:");
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Showcard Gothic", Font.PLAIN, 12));
		lblNewLabel.setBounds(332, 225, 78, 23);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Primer letra");
		lblNewLabel_1.setForeground(Color.ORANGE);
		lblNewLabel_1.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		lblNewLabel_1.setBounds(332, 201, 105, 20);
		contentPane.add(lblNewLabel_1);


		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 500, 500);
		contentPane.add(labelFondo);
		

	}

	public ControladorPreguntasNueva getCpn() {
		return cpn;
	}

	public void setCpn(ControladorPreguntasNueva cpn) {
		this.cpn = cpn;
	}

	public JTextField getTextFieldPregunta() {
		return textFieldPregunta;
	}

	public void setTextFieldPregunta(JTextField textFieldPregunta) {
		this.textFieldPregunta = textFieldPregunta;
	}

	public JTextField getTextFieldRespuesta() {
		return textFieldRespuesta;
	}

	public void setTextFieldRespuesta(JTextField textFieldRespuesta) {
		this.textFieldRespuesta = textFieldRespuesta;
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public void setBtnAgregar(JButton btnAgregar) {
		this.btnAgregar = btnAgregar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JRadioButton getRdbtnContiene() {
		return rdbtnContiene;
	}

	public void setRdbtnContiene(JRadioButton rdbtnContiene) {
		this.rdbtnContiene = rdbtnContiene;
	}

	public JRadioButton getRdbtnEmpiezaCon() {
		return rdbtnEmpiezaCon;
	}

	public void setRdbtnEmpiezaCon(JRadioButton rdbtnEmpiezaCon) {
		this.rdbtnEmpiezaCon = rdbtnEmpiezaCon;
	}

	public JComboBox getComboBoxLetra() {
		return comboBoxLetra;
	}

	public void setComboBoxLetra(JComboBox comboBoxLetra) {
		this.comboBoxLetra = comboBoxLetra;
	}
}
