package vista;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorVistaPrincipal;

public class vistaPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ControladorVistaPrincipal cvp;
	private JButton btnOpcionesDeJuego;
	private JButton btnRanking;
	private JButton btnSalir;

	public vistaPrincipal(ControladorVistaPrincipal cvp) {

		this.setCvp(cvp);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 600);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\menu.png").getImage().getScaledInstance(500, 600, Image.SCALE_DEFAULT));

		btnOpcionesDeJuego = new JButton("Comenzar");
		btnOpcionesDeJuego.setForeground(new Color(255, 255, 0));
		btnOpcionesDeJuego.setFont(new Font("Showcard Gothic", Font.PLAIN, 20));
		btnOpcionesDeJuego.setBackground(new Color(139, 0, 139));
		btnOpcionesDeJuego.addActionListener(this.getCvp());

	

		btnRanking = new JButton("Ranking");
		btnRanking.setForeground(new Color(255, 255, 0));
		btnRanking.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnRanking.setBackground(new Color(139, 0, 139));
		btnRanking.addActionListener(this.getCvp());

		btnSalir = new JButton("Salir");
		btnSalir.setForeground(new Color(255, 255, 0));
		btnSalir.setFont(new Font("Showcard Gothic", Font.PLAIN, 13));
		btnSalir.setBackground(new Color(139, 0, 139));
		btnSalir.addActionListener(this.getCvp());
		btnSalir.setBounds(205, 492, 89, 35);
		contentPane.add(btnSalir);
		btnRanking.setBounds(151, 418, 184, 35);
		contentPane.add(btnRanking);

		btnOpcionesDeJuego.setBounds(151, 300, 184, 78);

		contentPane.add(btnOpcionesDeJuego);

		JLabel labelFondo = new JLabel("");
		labelFondo.setForeground(Color.WHITE);
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 500, 600);
		contentPane.add(labelFondo);

		repaint();
	}

	public ControladorVistaPrincipal getCvp() {
		return cvp;
	}

	public void setCvp(ControladorVistaPrincipal cvp) {
		this.cvp = cvp;
	}



	public JButton getBtnOpcionesDeJuego() {
		return btnOpcionesDeJuego;
	}

	public void setBtnOpcionesDeJuego(JButton btnOpcionesDeJuego) {
		this.btnOpcionesDeJuego = btnOpcionesDeJuego;
	}

	public JButton getBtnRanking() {
		return btnRanking;
	}

	public void setBtnRanking(JButton btnRanking) {
		this.btnRanking = btnRanking;
	}

	public JButton getBtnSalir() {
		return btnSalir;
	}

	public void setBtnSalir(JButton btnSalir) {
		this.btnSalir = btnSalir;
	}

}
