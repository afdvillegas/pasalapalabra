package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorRanking;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import java.awt.Color;

public class vistaRanking extends JFrame {

	private static final long serialVersionUID = 1L;
	private JButton btnvolver;
	private JTable table;
	private DefaultTableModel modelotabla;
	private JScrollPane scrollPane;
	private ControladorRanking cr;
	private JButton btnTopJugadores;
	private JButton btnReporte;

	public vistaRanking(ControladorRanking cr) {
		this.setCr(cr);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 600);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		this.setLocationRelativeTo(null);

		btnvolver = new JButton("Volver");
		btnvolver.setBackground(new Color(139, 0, 139));
		btnvolver.setForeground(new Color(255, 140, 0));
		btnvolver.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		btnvolver.setBounds(207, 515, 103, 35);
		btnvolver.addActionListener(this.getCr());
		contentPane.add(btnvolver);

		ImageIcon imageIcon = new ImageIcon(
				new ImageIcon("Fondo\\rankig.png").getImage().getScaledInstance(500, 600, Image.SCALE_DEFAULT));

		scrollPane = new JScrollPane();
		scrollPane.setBounds(79, 104, 344, 386);
		contentPane.add(scrollPane);

		modelotabla = new DefaultTableModel(new Object[][] {},
				new String[] { "    Jugador", "    Victorias", "   Derrotas", "   Puntuacion" });
		table = new JTable();
		table.setModel(modelotabla);
		table.setBounds(375, 216, -295, -146);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDefaultEditor(Object.class, null);
		scrollPane.setViewportView(table);

		btnTopJugadores = new JButton("Top");
		btnTopJugadores.setForeground(new Color(255, 140, 0));
		btnTopJugadores.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		btnTopJugadores.setBackground(new Color(139, 0, 139));
		btnTopJugadores.setBounds(320, 515, 103, 35);
		btnTopJugadores.addActionListener(this.getCr());
		contentPane.add(btnTopJugadores);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setForeground(new Color(255, 140, 0));
		btnReporte.setFont(new Font("Showcard Gothic", Font.PLAIN, 16));
		btnReporte.setBackground(new Color(139, 0, 139));
		btnReporte.setBounds(82, 515, 115, 35);
		btnReporte.addActionListener(this.getCr());
		contentPane.add(btnReporte);
		
		JLabel labelFondo = new JLabel("");
		labelFondo.setIcon(imageIcon);
		contentPane.add(labelFondo);
		labelFondo.setBounds(0, 0, 500, 600);
		contentPane.add(labelFondo);
		
	
		

		
	}

	public ControladorRanking getCr() {
		return cr;
	}

	public void setCr(ControladorRanking cr) {
		this.cr = cr;
	}

	public JButton getBtnvolver() {
		return btnvolver;
	}

	public void setBtnvolver(JButton btnvolver) {
		this.btnvolver = btnvolver;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public DefaultTableModel getModelotabla() {
		return modelotabla;
	}

	public void setModelotabla(DefaultTableModel modelotabla) {
		this.modelotabla = modelotabla;
	}

	public JButton getBtnTopJugadores() {
		return btnTopJugadores;
	}

	public void setBtnTopJugadores(JButton btnTopJugadores) {
		this.btnTopJugadores = btnTopJugadores;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public void setBtnReporte(JButton btnReporte) {
		this.btnReporte = btnReporte;
	}
	
	
}
