package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorReportePreguntas;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class vistaReportePreguntas extends JFrame {

	private JPanel contentPane;
	private ControladorReportePreguntas crp;
	private JComboBox<Character> comboBoxLetra;
	private JButton btnGenerar;


	public vistaReportePreguntas(ControladorReportePreguntas crp) {
		this.setCrp(crp);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 327, 108);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGeneraInformePor = new JLabel("Genera informe de preguntas por letra ingresada");
		lblGeneraInformePor.setBounds(10, 11, 301, 14);
		contentPane.add(lblGeneraInformePor);
		
		btnGenerar = new JButton("Generar");
		btnGenerar.setBounds(122, 36, 89, 23);
		btnGenerar.addActionListener(this.getCrp());
		contentPane.add(btnGenerar);
		
		comboBoxLetra = new JComboBox<Character>();
		comboBoxLetra.setToolTipText("");
		comboBoxLetra.setBounds(46, 36, 44, 20);
		comboBoxLetra.addItem('A');
		comboBoxLetra.addItem('B');
		comboBoxLetra.addItem('C');
		comboBoxLetra.addItem('D');
		comboBoxLetra.addItem('E');
		comboBoxLetra.addItem('F');
		comboBoxLetra.addItem('G');
		comboBoxLetra.addItem('H');
		comboBoxLetra.addItem('I');
		comboBoxLetra.addItem('J');
		comboBoxLetra.addItem('K');
		comboBoxLetra.addItem('L');
		comboBoxLetra.addItem('M');
		comboBoxLetra.addItem('N');
		comboBoxLetra.addItem('O');
		comboBoxLetra.addItem('P');
		comboBoxLetra.addItem('Q');
		comboBoxLetra.addItem('R');
		comboBoxLetra.addItem('T');
		comboBoxLetra.addItem('U');
		comboBoxLetra.addItem('V');
		comboBoxLetra.addItem('W');
		comboBoxLetra.addItem('X');
		comboBoxLetra.addItem('Y');
		comboBoxLetra.addItem('Z');
		contentPane.add(comboBoxLetra);
	}


	public ControladorReportePreguntas getCrp() {
		return crp;
	}


	public void setCrp(ControladorReportePreguntas crp) {
		this.crp = crp;
	}


	public JComboBox getComboBoxLetra() {
		return comboBoxLetra;
	}


	public void setComboBoxLetra(JComboBox comboBoxLetra) {
		this.comboBoxLetra = comboBoxLetra;
	}


	public JButton getBtnGenerar() {
		return btnGenerar;
	}


	public void setBtnGenerar(JButton btnGenerar) {
		this.btnGenerar = btnGenerar;
	}
	
	
}
